﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NewStyleAccounting.GUIL
{
    public partial class FieldBoxes : Form
    {
        CLASSES.FieldBoxes fi = new CLASSES.FieldBoxes();
        CLASSES.Suppliers s = new CLASSES.Suppliers();
        public FieldBoxes()
        {
            InitializeComponent();
            cmbEmpName.DataSource = s.getAll();
            cmbEmpName.DisplayMember = "الاسم";
            cmbEmpName.ValueMember = "الاسم";
        }
        
        private void FieldBoxes_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = fi.getAll();
            textBox2.Text = "0";
        }

        private void cmbEmpName_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                fi.Add(cmbEmpName.SelectedValue.ToString(), Convert.ToInt32(textBox2.Text), this.dateTimePicker1.Value);                
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            dataGridView1.DataSource = fi.getAll();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                int numOfBoxes = Convert.ToInt32(dataGridView1.CurrentRow.Cells[1].Value);
                fi.Return(dataGridView1.CurrentRow.Cells[0].Value.ToString(), Convert.ToInt32(textBox2.Text), dateTimePicker1.Value);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            dataGridView1.DataSource = fi.getAll();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            dataGridView1.DataSource = fi.Search(textBox1.Text);            
        }
    }
}
