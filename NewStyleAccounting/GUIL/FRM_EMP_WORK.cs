﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NewStyleAccounting.GUIL
{
    public partial class FRM_EMP_WORK : Form
    {

        CLASSES.Employee emp = new CLASSES.Employee();

        public FRM_EMP_WORK()
        {
            InitializeComponent();
            cmbEmpName.DataSource = emp.getAllEmp();
            cmbEmpName.DisplayMember = "الاسم";
            cmbEmpName.ValueMember = "الاسم";
            // force the cmbo box to select its first item
            cmbDaytype.SelectedIndex = 0;
        }


        private void FRM_EMP_WORK_Load(object sender, EventArgs e)
        {
            txtMulFact.Text = Properties.Settings.Default.MulFactor.ToString();
            textBox1.Text = "0";
            dataGridView1.DataSource = emp.getWorkData();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("إضافة يوم عمل", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                try
                {
                    double EmpSal = Convert.ToDouble(emp.getEmpSal(cmbEmpName.SelectedValue.ToString()).Rows[0][0].ToString());
                    double DaySal = 0; // the tottal salary of this day
                    int mulFactor;
                    bool FullDay;
                    int ExtraHrs = Convert.ToInt32(textBox1.Text.ToString());
                    double Ratio = Convert.ToDouble(txtMulFact.Text.ToString());
                    if (cmbDaytype.SelectedIndex == 0)
                    {
                        mulFactor = 8;
                        FullDay = true;
                    }
                    else
                    {
                        mulFactor = 4;
                        FullDay = false;
                    }

                    DaySal = Convert.ToDouble((EmpSal / 240) * (mulFactor + ExtraHrs * Ratio));

                    emp.addWorkDay(cmbEmpName.SelectedValue.ToString(), ExtraHrs, DaySal, FullDay, dateTimePicker1.Value);
                    MessageBox.Show("تمت العملية بنجاح", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message, "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    return;
                }
            }            
            dataGridView1.DataSource = emp.getWorkData();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            RPT.FRM_RPT frm = new RPT.FRM_RPT();
            RPT.rpt_work r = new RPT.rpt_work();
            r.SetDataSource(emp.getWorkData());
            frm.crystalReportViewer1.ReportSource = r;
            frm.ShowDialog();

            /* for a specific user to calculate his payment
             * RPT.FRM_RPT frm = new RPT.FRM_RPT();
            RPT.rpt_work r = new RPT.rpt_work();
            r.SetDataSource(emp.getWorkData("Amjad"));
            frm.crystalReportViewer1.ReportSource = r;
            frm.ShowDialog();
             */
        }
    }
}
