﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NewStyleAccounting.GUIL
{
    public partial class FRM_ManageUsers : Form
    {
        CLASSES.Users usr = new CLASSES.Users();

        public FRM_ManageUsers()
        {
            InitializeComponent();
            this.dgUsers.DataSource = usr.getAllUsers();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            new GUIL.AddUser().ShowDialog();
            this.dgUsers.DataSource = usr.getAllUsers();
        }

        private void FRM_ManageUsers_Load(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            this.dgUsers.DataSource = usr.SearchUsers(textBox1.Text);
        }

        private void btnDel_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("هل تريد الحذف", "تأكيد الحذف", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation)==DialogResult.Yes)
            {
                usr.DeleteUser(this.dgUsers.CurrentRow.Cells[0].Value.ToString());
                this.dgUsers.DataSource = usr.getAllUsers();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Dispose();
        }
    }
}
