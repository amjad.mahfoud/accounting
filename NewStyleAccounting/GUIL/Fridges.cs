﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NewStyleAccounting.GUIL
{
    public partial class Fridges : Form
    {
        CLASSES.Traders trader = new CLASSES.Traders();
        CLASSES.Fridge fr = new CLASSES.Fridge();
        public Fridges()
        {
            InitializeComponent();
            cmbEmpName.DataSource = trader.getAll();
            cmbEmpName.DisplayMember = "الاسم";
            cmbEmpName.ValueMember = "الاسم";
        }

        private void Fridges_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = fr.getAll();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                fr.Add(cmbEmpName.SelectedValue.ToString(), txtTruck.Text, txtDriver.Text, txtP1.Text, txtP2.Text, Convert.ToDouble(txtC.Text), this.dateTimePicker1.Value);
                txtC.Text = "0";
                txtDriver.Text = "";
                txtP1.Text = "";
                txtP2.Text = "";
                txtTruck.Text = "";
                cmbEmpName.Focus();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            dataGridView1.DataSource = fr.getAll();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            dataGridView1.DataSource = fr.Search(textBox1.Text);
        }

        public static int fId = 0;

        private void عرضالمحتوياتToolStripMenuItem_Click(object sender, EventArgs e)
        {

            try
            {
                fId = Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value.ToString());
                new GUIL.Fridges_Contents().ShowDialog();
            }
            catch (Exception)
            {
                MessageBox.Show("Please select a cell in the table", "Error");
            }
            this.dataGridView1.DataSource = fr.getAll();
        }

        private void إضافةمكوناتToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(dataGridView1.CurrentRow.Cells[11].Value.ToString()) != 0)
                {
                    MessageBox.Show("You can't add more data, the fridge is already shipd", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    return;
                }
                fId = Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value.ToString());
                new GUIL.FridgeContentsAdd().ShowDialog();
            }
            catch (Exception)
            {
                MessageBox.Show("Please select a cell in the table", "Error");
            }
            this.dataGridView1.DataSource = fr.getAll();
        }

        private void طباعةالمكوناتToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                fId = Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value.ToString());                
                RPT.FRM_RPT frm = new RPT.FRM_RPT();
                RPT.rpt_fridge_contents r = new RPT.rpt_fridge_contents();
                r.SetDataSource(fr.Print(fId));
                frm.crystalReportViewer1.ReportSource = r;
                frm.ShowDialog();
            }
            catch (Exception)
            {
                 MessageBox.Show("Please select a cell in the table", "Error");                
            }
        }
    }
}
