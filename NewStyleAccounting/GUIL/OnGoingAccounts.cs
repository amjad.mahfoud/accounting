﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NewStyleAccounting.GUIL
{
    public partial class OnGoingAccounts : Form
    {
        public OnGoingAccounts()
        {
            InitializeComponent();
        }
        CLASSES.CHoleTradeAcc oAcc = new CLASSES.CHoleTradeAcc();
        private void OnGoingAccounts_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = oAcc.get();
        }

        private void dataGridView1_Click(object sender, EventArgs e)
        {
            Dispose();
        }
    }
}
