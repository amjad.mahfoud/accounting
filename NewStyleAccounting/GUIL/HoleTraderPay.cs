﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NewStyleAccounting.GUIL
{
    public partial class HoleTraderPay : Form
    {
        public HoleTraderPay()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Dispose();
        }
        CLASSES.SupTrad sup = new CLASSES.SupTrad();
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                double paid = GUIL.HoleTradeOut.paid;
                int id = GUIL.HoleTradeOut.FID;
                double total = GUIL.HoleTradeOut.total;
                double ammount = Convert.ToDouble(textBox1.Text);
                if (ammount > total - paid)
                {
                    MessageBox.Show("The Ammount is greater than the dept", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
                else                
                {
                    sup.pay(id, ammount);
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            Dispose();
        }
    }
}
