﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NewStyleAccounting.GUIL
{
    public partial class SuppliesPay : Form
    {
        CLASSES.Supplies supplies = new CLASSES.Supplies();
        public SuppliesPay()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        int SupId = GUIL.Supplies.SupId;
        double SupFull = GUIL.Supplies.SupTotal;
        double SupPaid = GUIL.Supplies.SupPaid;

        private void button1_Click(object sender, EventArgs e)
        {
            
            if (textBox1.Text == "")
            {
                MessageBox.Show("Please Complete all fields", "Empty Fiekd", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                double Amm = Convert.ToDouble(textBox1.Text);

                if (Amm + SupPaid > SupFull)
                {
                    MessageBox.Show("The Ammount is greater than the dept", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
                else
                {
                    try
                    {
                        supplies.Pay(SupId, Amm);
                        Dispose();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    }
                }
            }
        }

        private void SuppliesPay_Load(object sender, EventArgs e)
        {
            textBox1.Focus();
        }
    }
}
