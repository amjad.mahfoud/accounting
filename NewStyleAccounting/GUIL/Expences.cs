﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NewStyleAccounting.GUIL
{
    public partial class Expences : Form
    {
        public Expences()
        {
            InitializeComponent();
        }

        CLASSES.Expences ex = new CLASSES.Expences();

        private void Expences_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = ex.getAll();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {                
                ex.addEmp(textBox2.Text, textBox3.Text, Convert.ToDouble(textBox1.Text), dateTimePicker1.Value);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            dataGridView1.DataSource = ex.getAll();
        }

        private void حذفToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("تأكيد حذف", "النفقات", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    ex.del(Convert.ToInt32(this.dataGridView1.CurrentRow.Cells[0].Value));
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            dataGridView1.DataSource = ex.getAll();
        }
    }
}
