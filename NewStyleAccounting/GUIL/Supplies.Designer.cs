﻿namespace NewStyleAccounting.GUIL
{
    partial class Supplies
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbEmpName = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.إضافةمكوناتToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.تشميعالكميةToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.دفعمبلغToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.طباعةالمكوناتToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.عرضالمحتوياتToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.button1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(704, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 15;
            this.label1.Text = "المورد";
            // 
            // cmbEmpName
            // 
            this.cmbEmpName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbEmpName.FormattingEnabled = true;
            this.cmbEmpName.Location = new System.Drawing.Point(526, 60);
            this.cmbEmpName.Name = "cmbEmpName";
            this.cmbEmpName.Size = new System.Drawing.Size(172, 21);
            this.cmbEmpName.Sorted = true;
            this.cmbEmpName.TabIndex = 14;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.textBox1);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.dataGridView1);
            this.groupBox2.Location = new System.Drawing.Point(12, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(489, 254);
            this.groupBox2.TabIndex = 16;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "قائمة الحمولات";
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(108, 21);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(213, 21);
            this.textBox1.TabIndex = 2;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(327, 24);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(27, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "يحث";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.ContextMenuStrip = this.contextMenuStrip1;
            this.dataGridView1.Location = new System.Drawing.Point(6, 48);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(477, 206);
            this.dataGridView1.TabIndex = 0;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.إضافةمكوناتToolStripMenuItem,
            this.تشميعالكميةToolStripMenuItem,
            this.دفعمبلغToolStripMenuItem,
            this.طباعةالمكوناتToolStripMenuItem,
            this.عرضالمحتوياتToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.contextMenuStrip1.Size = new System.Drawing.Size(155, 114);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // إضافةمكوناتToolStripMenuItem
            // 
            this.إضافةمكوناتToolStripMenuItem.Name = "إضافةمكوناتToolStripMenuItem";
            this.إضافةمكوناتToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.إضافةمكوناتToolStripMenuItem.Text = "إضافة مكونات";
            this.إضافةمكوناتToolStripMenuItem.Click += new System.EventHandler(this.إضافةمكوناتToolStripMenuItem_Click);
            // 
            // تشميعالكميةToolStripMenuItem
            // 
            this.تشميعالكميةToolStripMenuItem.Name = "تشميعالكميةToolStripMenuItem";
            this.تشميعالكميةToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.تشميعالكميةToolStripMenuItem.Text = "تشميع الكمية";
            this.تشميعالكميةToolStripMenuItem.Click += new System.EventHandler(this.تشميعالكميةToolStripMenuItem_Click);
            // 
            // دفعمبلغToolStripMenuItem
            // 
            this.دفعمبلغToolStripMenuItem.Name = "دفعمبلغToolStripMenuItem";
            this.دفعمبلغToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.دفعمبلغToolStripMenuItem.Text = "دفع مبلغ";
            this.دفعمبلغToolStripMenuItem.Click += new System.EventHandler(this.دفعمبلغToolStripMenuItem_Click);
            // 
            // طباعةالمكوناتToolStripMenuItem
            // 
            this.طباعةالمكوناتToolStripMenuItem.Name = "طباعةالمكوناتToolStripMenuItem";
            this.طباعةالمكوناتToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.طباعةالمكوناتToolStripMenuItem.Text = "طباعة المكونات";
            this.طباعةالمكوناتToolStripMenuItem.Click += new System.EventHandler(this.طباعةالمكوناتToolStripMenuItem_Click);
            // 
            // عرضالمحتوياتToolStripMenuItem
            // 
            this.عرضالمحتوياتToolStripMenuItem.Name = "عرضالمحتوياتToolStripMenuItem";
            this.عرضالمحتوياتToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.عرضالمحتوياتToolStripMenuItem.Text = "عرض المحتويات";
            this.عرضالمحتوياتToolStripMenuItem.Click += new System.EventHandler(this.عرضالمحتوياتToolStripMenuItem_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(526, 113);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(172, 23);
            this.button1.TabIndex = 17;
            this.button1.Text = "توريد حمولة";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(673, 234);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 13);
            this.label2.TabIndex = 18;
            this.label2.Text = "كلفة التشميع";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(526, 231);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(141, 20);
            this.textBox2.TabIndex = 19;
            this.textBox2.Text = "3.5";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(704, 87);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 13);
            this.label7.TabIndex = 21;
            this.label7.Text = "التاريخ";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker1.Location = new System.Drawing.Point(526, 87);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.RightToLeftLayout = true;
            this.dateTimePicker1.Size = new System.Drawing.Size(172, 20);
            this.dateTimePicker1.TabIndex = 20;
            // 
            // Supplies
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(743, 270);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmbEmpName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Supplies";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "الحمولات";
            this.Load += new System.EventHandler(this.Supplies_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbEmpName;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem إضافةمكوناتToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem تشميعالكميةToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem دفعمبلغToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem طباعةالمكوناتToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem عرضالمحتوياتToolStripMenuItem;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
    }
}