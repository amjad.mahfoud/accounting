﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NewStyleAccounting.GUIL
{
    public partial class TranMony : Form
    {
        public TranMony()
        {
            InitializeComponent();
            cmbEmpName.DataSource = fr.getAll();
            cmbEmpName.DisplayMember = "الرقم";
            cmbEmpName.ValueMember = "الرقم";
            dataGridView1.DataSource = fr.getTranMony();
        }
        CLASSES.Fridge fr = new CLASSES.Fridge();

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                int fid = Convert.ToInt32(cmbEmpName.SelectedValue.ToString());                
                double aSp = Convert.ToDouble(textBox1.Text);
                double aUS = Convert.ToDouble(textBox2.Text);
                double usToSp = Convert.ToDouble(textBox3.Text);

                if (aUS != 0 && aSp==0)
                {
                    aSp = (double) aUS * usToSp;
                }
                else if (aSp != 0 && aUS == 0)
                {
                    aUS = (double) aSp / usToSp;
                }

                fr.AddTranMony(fid, textBox4.Text, aSp, aUS, usToSp);
                textBox1.Text = "0";
                textBox2.Text = "0";
                textBox3.Text = "1";
                textBox4.Text = "0";

                textBox1.Focus();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error");
                return;
            }
            dataGridView1.DataSource = fr.getTranMony();
            // fr.AddTranMony(
        }
    }
}
