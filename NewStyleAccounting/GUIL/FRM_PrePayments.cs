﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NewStyleAccounting.GUIL
{
    public partial class FRM_PrePayments : Form
    {
        CLASSES.Employee emp = new CLASSES.Employee();
        public FRM_PrePayments()
        {
            InitializeComponent();
            cmbEmpName.DataSource = emp.getAllEmp();
            cmbEmpName.DisplayMember = "الاسم";
            cmbEmpName.ValueMember = "الاسم";                        
        }

        private void FRM_PrePayments_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = emp.getAllPrePayments();
            textBox1.Text = "0";            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                double num = Convert.ToDouble(textBox1.Text);
                emp.AddPrePayment(cmbEmpName.SelectedValue.ToString(), num, dateTimePicker1.Value);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            dataGridView1.DataSource = emp.getAllPrePayments();            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                emp.ReturnPrePayment(Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value.ToString()));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            dataGridView1.DataSource = emp.getAllPrePayments();            
        }

        private void تسديدسلفةToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                emp.ReturnPrePayment(Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value.ToString()));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            dataGridView1.DataSource = emp.getAllPrePayments();            
        }
    }
}
