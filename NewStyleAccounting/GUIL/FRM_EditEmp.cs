﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NewStyleAccounting.GUIL
{
    public partial class FRM_EditEmp : Form
    {
        public FRM_EditEmp()
        {
            InitializeComponent();
            txtName.Text = GUIL.FRM_AddEmp.EmpName;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Dispose();
        }
        CLASSES.Employee emp = new CLASSES.Employee();
        private void button1_Click(object sender, EventArgs e)
        {
            if (txtSal.Text.Equals("") || txtName.Text.Equals(""))
            {
                MessageBox.Show("حقل المرتب فارغ", "حقول اساسية فارغة", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            try
            {
                Int64 num = Int64.Parse(txtSal.Text);
                emp.UpdateEmp(txtName.Text, txtPhone.Text, txtMob.Text, txtAdd.Text, num);
                MessageBox.Show("تم تعديل الموظف بنجاح", "تعديل موظف", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Dispose();
            }
            catch (Exception empex)
            {
                MessageBox.Show(empex.Message, "تعديل موظف", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
        }

        private void FRM_EditEmp_Load(object sender, EventArgs e)
        {

        }
    }
}
