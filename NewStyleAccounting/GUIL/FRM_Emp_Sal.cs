﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NewStyleAccounting.GUIL
{
    public partial class FRM_Emp_Sal : Form
    {
        CLASSES.Employee emp = new CLASSES.Employee();

        public FRM_Emp_Sal()
        {
            InitializeComponent();
        }

        private void FRM_Emp_Sal_Load(object sender, EventArgs e)
        {
            cmbEmpName.DataSource = emp.getAllEmp();
            cmbEmpName.DisplayMember = "الاسم";
            cmbEmpName.ValueMember = "الاسم";
            dataGridView1.DataSource = emp.getSalaries();
            //emp.getWorkData(cmbEmpName.SelectedValue.ToString());
            dataGridView1.Columns[0].Visible = false;                   
        }

        private void cmbEmpName_SelectedIndexChanged(object sender, EventArgs e)
        {
            //dataGridView1.Columns[1].Visible = false;
            dataGridView1.DataSource = emp.getSalaries(cmbEmpName.SelectedValue.ToString());
        }

        private void button1_Click(object sender, EventArgs e)
        {            
            try
            {
                if (MessageBox.Show("تسديد مرتب", "", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.Yes)
                {
                    int EmpId = Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value.ToString());
                    emp.PaySal(EmpId);
                    //s.dgUsers.CurrentRow.Cells[0].Value.ToString());
                    dataGridView1.DataSource = emp.getSalaries();
                }
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("الرحاء اختيار موظف","",MessageBoxButtons.OK,MessageBoxIcon.Question);
                return;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                RPT.FRM_RPT frm = new RPT.FRM_RPT();
                RPT.rpt_sal_details r = new RPT.rpt_sal_details();
                DateTime dt = Convert.ToDateTime(dataGridView1.CurrentRow.Cells[2].Value);
                r.SetDataSource(emp.getWorkData(dataGridView1.CurrentRow.Cells[1].Value.ToString(),dt));
                frm.crystalReportViewer1.ReportSource = r;
                frm.ShowDialog();
            }
            catch (Exception)
            {
                MessageBox.Show("Please select a field in the table", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                return;
            }
        }
    }
}
