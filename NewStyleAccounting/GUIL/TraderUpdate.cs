﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NewStyleAccounting.GUIL
{
    public partial class TraderUpdate : Form
    {
        public TraderUpdate()
        {
            InitializeComponent();
        }

        private void TraderUpdate_Load(object sender, EventArgs e)
        {
            txtName.Text = GUIL.Traders.name;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Dispose();
        }
        CLASSES.Traders trader = new CLASSES.Traders();
        private void button1_Click(object sender, EventArgs e)
        {
            trader.Update(txtName.Text, txtPhone.Text, txtMob.Text, txtAdd.Text);
            Dispose();
        }
    }
}
