﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NewStyleAccounting.GUIL
{
    public partial class HoleTradInViewContents : Form
    {
        public HoleTradInViewContents()
        {
            InitializeComponent();
        }
        CLASSES.CInContents cic = new CLASSES.CInContents();
        private void HoleTradInViewContents_Load(object sender, EventArgs e)
        {
            int id = GUIL.HoleTraderIn.FID;
            dataGridView1.DataSource = cic.get(id);
        }

        private void dataGridView1_Click(object sender, EventArgs e)
        {
            Dispose();
        }
    }
}
