﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace NewStyleAccounting.GUIL
{
    public partial class FRM_AddEmp : Form
    {
        public FRM_AddEmp()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Dispose();
        }
        CLASSES.Employee emp = new CLASSES.Employee();
        public static string name = "";

        private void button1_Click(object sender, EventArgs e)
        {
            if (txtSal.Text.Equals("") || txtName.Text.Equals(""))
            {
                MessageBox.Show("حقلي الاسم والمرتب فارغين", "حقول اساسية فارغة", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            if (MessageBox.Show("تأكيد إضافة موظف", "إضافة موظف",MessageBoxButtons.YesNo,MessageBoxIcon.Question) == DialogResult.Yes)
            {
                try
                {
                    Int64 num = Int64.Parse(txtSal.Text);
                    emp.addEmp(txtName.Text, txtPhone.Text, txtMob.Text, txtAdd.Text, num, dateTimePicker1.Value);
                    MessageBox.Show("تم إضافة الموظف بنجاح", "إضافة موظف", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtName.Text = "";
                    txtAdd.Text = "";
                    txtMob.Text = "";
                    txtPhone.Text = "";
                    txtSal.Text = "";
                }
                catch (SqlException)
                {
                    MessageBox.Show("الموظف موجود مسبفا", "إضافة موظف", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    txtName.Focus();
                    txtName.SelectionStart = 0;
                    txtName.SelectionLength = txtName.TextLength;
                    return;
                }
                catch (Exception addExc)
                {
                    MessageBox.Show(addExc.Message, "إضافة موظف", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
            }
            dataGridView1.DataSource = emp.getAllEmp();
        }

        private void FRM_AddEmp_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = emp.getAllEmp();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        public static string EmpName
        {
            get
            {
                return name;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {            
            try
            {
                name = this.dataGridView1.CurrentRow.Cells[0].Value.ToString();
                GUIL.FRM_EditEmp frm = new FRM_EditEmp();
                frm.ShowDialog();
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("Please select an employee to edit","Notification", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            dataGridView1.DataSource = emp.getAllEmp();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            RPT.FRM_RPT frm = new RPT.FRM_RPT();
            RPT.rpt_emp r = new RPT.rpt_emp();
            r.SetDataSource(emp.getAllEmp());
            frm.crystalReportViewer1.ReportSource = r;
            frm.ShowDialog();
        }
    }
}
