﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NewStyleAccounting.GUIL
{
    public partial class FridgeContentsAdd : Form
    {
        public FridgeContentsAdd()
        {
            InitializeComponent();
        }

        CLASSES.Fridge fr = new CLASSES.Fridge();        

        private void FridgeContentsAdd_Load(object sender, EventArgs e)
        {
            try
            {
                int id = GUIL.Fridges.fId;
                textBox1.Text = id.ToString();
                this.dataGridView1.DataSource = fr.getContents(id);
            }
            catch (Exception)
            {
                return;
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(textBox1.Text);
            try
            {                
                int qty = Convert.ToInt32(textBox3.Text);
                double price = Convert.ToDouble(textBox4.Text);
                fr.AddContent(id, textBox2.Text, qty, price);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK,MessageBoxIcon.Hand);
            }
            dataGridView1.DataSource = fr.getContents(id);
        }
    }
}
