﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NewStyleAccounting.GUIL
{
    public partial class SupTradUpdate : Form
    {
        public SupTradUpdate()
        {
            InitializeComponent();
        }

        private void SupTradUpdate_Load(object sender, EventArgs e)
        {
            txtName.Text = GUIL.SupTrad.name;
        }
      
        CLASSES.SupTrad trader = new CLASSES.SupTrad();       
        private void button1_Click_1(object sender, EventArgs e)
        {
            trader.Update(txtName.Text, txtPhone.Text, txtMob.Text, txtAdd.Text);
            Dispose();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            Dispose();
        }
    }
}
