﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NewStyleAccounting.GUIL
{
    public partial class Reports : Form
    {

        CLASSES.CReports orep = new CLASSES.CReports();
        public Reports()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                RPT.FRM_RPT frm = new RPT.FRM_RPT();
                RPT.FBD2 r = new RPT.FBD2();
                r.SetDataSource(orep.getFieldBixes(dtfrom.Value, dtto.Value));
                frm.crystalReportViewer1.ReportSource = r;
                frm.ShowDialog();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                RPT.FRM_RPT frm = new RPT.FRM_RPT();
                RPT.PPBD2 r = new RPT.PPBD2();
                r.SetDataSource(orep.getPrePaid(dtfrom.Value, dtto.Value));
                frm.crystalReportViewer1.ReportSource = r;
                frm.ShowDialog();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                RPT.FRM_RPT frm = new RPT.FRM_RPT();
                RPT.SBD2 r = new RPT.SBD2();
                r.SetDataSource(orep.getSuppliedByDate(dtfrom.Value, dtto.Value));
                frm.crystalReportViewer1.ReportSource = r;
                frm.ShowDialog();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                RPT.FRM_RPT frm = new RPT.FRM_RPT();
                RPT.FrBD2 r = new RPT.FrBD2();
                r.SetDataSource(orep.getFridgesByDate(dtfrom.Value, dtto.Value));
                frm.crystalReportViewer1.ReportSource = r;
                frm.ShowDialog();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
                RPT.FRM_RPT frm = new RPT.FRM_RPT();
                RPT.EBD2 r = new RPT.EBD2();
                r.SetDataSource(orep.getExpencesByDate(dtfrom.Value, dtto.Value));
                frm.crystalReportViewer1.ReportSource = r;
                frm.ShowDialog();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            try
            {
                RPT.FRM_RPT frm = new RPT.FRM_RPT();
                RPT.SalBD2 r = new RPT.SalBD2();
                r.SetDataSource(orep.getSalByDate(dtfrom.Value, dtto.Value));
                frm.crystalReportViewer1.ReportSource = r;
                frm.ShowDialog();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            try
            {
                RPT.FRM_RPT frm = new RPT.FRM_RPT();
                RPT.WaxBD2 r = new RPT.WaxBD2();
                r.SetDataSource(orep.getWaxedByDate(dtfrom.Value, dtto.Value));
                frm.crystalReportViewer1.ReportSource = r;
                frm.ShowDialog();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            
        }

        private void button9_Click(object sender, EventArgs e)
        {
            try
            {
                RPT.FRM_RPT frm = new RPT.FRM_RPT();
                RPT.TMBD2 r = new RPT.TMBD2();
                r.SetDataSource(orep.getTransMonyByDate(dtfrom.Value, dtto.Value));
                frm.crystalReportViewer1.ReportSource = r;
                frm.ShowDialog();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }
    }
}
