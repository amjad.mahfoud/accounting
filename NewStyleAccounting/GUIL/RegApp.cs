﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HighTech;

namespace NewStyleAccounting.GUIL
{
    public partial class RegApp : Form
    {
        bool reg = Properties.Settings.Default.Registered;
        string sysID = Properties.Settings.Default.DeviceID;
        int numOfRuns = Properties.Settings.Default.NumberOfRuns;

        public RegApp()
        {
            InitializeComponent();
        }

        private void RegApp_Load(object sender, EventArgs e)
        {
            if (sysID.Equals(string.Empty))
            {
                ComputerID cid = new ComputerID();
                Properties.Settings.Default.DeviceID = cid.SysID;                
                Properties.Settings.Default.Save();
            }

            if (reg)
            {
                btnReg.Enabled = false;
                txtUserID.ReadOnly = true;
                txtUserID.Text = Properties.Settings.Default.RegName;
                txtSerial.Text = Properties.Settings.Default.RegSerial;
                txtSerial.ReadOnly = true;
            }
            else
            {
                txtUserID.Text = "Trial version, expires after 5 runs";
            }

            this.txtSysId.Text = Properties.Settings.Default.DeviceID;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (this.txtUserID.Text.ToString() == String.Empty)
            {
                MessageBox.Show("الرجاء إدخال إسم المستخدم بشكل صحيح", "اسم المستخدم خاطئ", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                return;
            }
            if (this.txtSerial.Text.Equals(""))
            {
                MessageBox.Show("الرجاء إدخال الرقم التسلسلي بشكل صحيح", "الرقم التسلسلي فارغ", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                return;
            }
            if (register())
            {
                MessageBox.Show("لقد تم تسجيل المنتج بنجاح", "شكرا لإختياركم منتجاتنا", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Properties.Settings.Default.Registered = true;
                Properties.Settings.Default.RegName = txtUserID.Text;
                Properties.Settings.Default.RegSerial = txtSerial.Text;
                Properties.Settings.Default.Save();
                this.Dispose();
            }
            else
            {
                MessageBox.Show("الرقم التسلسلي المدخل غير صالح!", "فشل التسجيل", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void txtSysId_TextChanged(object sender, EventArgs e)
        {
            
        }

        private bool register()
        {
            Tester tester = new Tester(this.txtSerial.Text, this.txtSysId.Text);
            return tester.test();
        }

        private void RegApp_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (numOfRuns >= 5 && !reg)
            {
                Form1.getMainForm.Dispose();
            }
        }
    }
}
