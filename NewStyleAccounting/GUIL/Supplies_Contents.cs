﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NewStyleAccounting.GUIL
{
    public partial class Supplies_Contents : Form
    {
        public Supplies_Contents()
        {
            InitializeComponent();
        }

        CLASSES.Supplies sup = new CLASSES.Supplies();

        private void Supplies_Contents_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = sup.getAllContents();
            int SupId = GUIL.Supplies.SupId;
            textBox1.Text = SupId.ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                int SupId = Convert.ToInt32(textBox1.Text);
                int qty = Convert.ToInt32(textBox3.Text);
                double price = Convert.ToDouble(textBox4.Text);
                sup.AddContent(SupId, textBox2.Text, qty, price, Convert.ToDouble(textBox5.Text));
                dataGridView1.DataSource = sup.getAllContents(SupId);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);                
            }            
        }
    }
}
