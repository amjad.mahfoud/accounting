﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NewStyleAccounting.GUIL
{
    public partial class HoleTraderIn : Form
    {
        public HoleTraderIn()
        {
            InitializeComponent();
        }
        CLASSES.SupTrad trader = new CLASSES.SupTrad();
        CLASSES.HoleTraderIn hto = new CLASSES.HoleTraderIn();
        private void HoleTraderIn_Load(object sender, EventArgs e)
        {
            cmbEmpName.DataSource = trader.getAll();
            cmbEmpName.DisplayMember = "الاسم";
            cmbEmpName.ValueMember = "الاسم";

            dataGridView1.DataSource = hto.getAll();
        }

        public static int FID = 0;
        public static double paid = 0;
        public static double total = 0;

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void إضافةمكوناتToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dataGridView1.CurrentRow.Cells[7].Value.ToString().Equals("True"))
            {
                MessageBox.Show("الكمية مشمعة لا يمكن الاضافة", "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            try
            {
                FID = Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value);
                paid = Convert.ToDouble(dataGridView1.CurrentRow.Cells[4].Value);
                total = Convert.ToDouble(dataGridView1.CurrentRow.Cells[2].Value);
                if (paid == total && paid != 0)
                {
                    MessageBox.Show("لا يمكن إضافة مكونات", "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else
                {
                    new GUIL.InContents().ShowDialog();
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            dataGridView1.DataSource = hto.getAll();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            try
            {
                hto.add(cmbEmpName.SelectedValue.ToString(), dateTimePicker1.Value);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            dataGridView1.DataSource = hto.getAll();
        }

        private void تشميعToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dataGridView1.CurrentRow.Cells[7].Value.ToString().Equals("True"))
            {
                MessageBox.Show("الكمية مشمعة بالفعل", "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                double aa = Convert.ToDouble(textBox2.Text);
                int id = Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value.ToString());
                hto.wax(id, aa);
            }
            dataGridView1.DataSource = hto.getAll();
        }

        private void عرضالمكوناتToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                FID = Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value);
                new GUIL.HoleTradInViewContents().ShowDialog();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }       
    }
}
