﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NewStyleAccounting.GUIL
{
    public partial class Waxed : Form
    {
        public Waxed()
        {
            InitializeComponent();
        }
        CLASSES.Wax w = new CLASSES.Wax();
        private void Waxed_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = w.getAll();
        }
        
        private void button1_Click_1(object sender, EventArgs e)
        {
            try
            {
                w.Add(Convert.ToDouble(textBox1.Text), Convert.ToDouble(textBox4.Text), 0, textBox2.Text);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
            dataGridView1.DataSource = w.getAll();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {                
                RPT.FRM_RPT frm = new RPT.FRM_RPT();
                RPT.rpt_wax r = new RPT.rpt_wax();
                r.SetDataSource(w.getAll());
                frm.crystalReportViewer1.ReportSource = r;
                frm.ShowDialog();
            }
            catch (Exception)
            {
                MessageBox.Show("Please select a cell in the table", "Error");
            }
        }
    }
}
