﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NewStyleAccounting.GUIL
{
    public partial class ViewOutContents : Form
    {
        CLASSES.COutContents oOut = new CLASSES.COutContents();
        public ViewOutContents()
        {
            InitializeComponent();
            dataGridView1.DataSource = oOut.get(Convert.ToInt32(GUIL.HoleTradeOut.FID.ToString()));
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            this.Dispose();
        }

        private void dataGridView1_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            
        }

        private void dataGridView1_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
        
    }
}
