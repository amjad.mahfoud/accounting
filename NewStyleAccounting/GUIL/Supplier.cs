﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace NewStyleAccounting.GUIL
{
    public partial class Supplier : Form
    {
        CLASSES.Suppliers sup = new CLASSES.Suppliers();
        public static string name = "";

        public Supplier()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (txtName.Text.Equals(""))
            {
                MessageBox.Show("حقل الاسم فارغ", "حقول اساسية فارغة", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            if (MessageBox.Show("تأكيد إضافة مورد", "إضافة مورد", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                try
                {
                    sup.Add(txtName.Text, txtPhone.Text, txtMob.Text, txtAdd.Text);
                    MessageBox.Show("تم إضافة المورد بنجاح", "إضافة مورد", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtName.Text = "";
                    txtAdd.Text = "";
                    txtMob.Text = "";
                    txtPhone.Text = "";
                    txtName.Focus();
                }
                catch (SqlException)
                {
                    MessageBox.Show("المورد موجود مسبفا", "إضافة مورد", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    txtName.Focus();
                    txtName.SelectionStart = 0;
                    txtName.SelectionLength = txtName.TextLength;
                    return;
                }
                catch (Exception addExc)
                {
                    MessageBox.Show(addExc.Message, "إضافة مورد", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
            }
            dataGridView1.DataSource = sup.getAll();
        }

        private void Supplier_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = sup.getAll();
        }

        public static string EmpName
        {
            get
            {
                return name;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                name = this.dataGridView1.CurrentRow.Cells[0].Value.ToString();
                GUIL.SupplierEdit frm = new SupplierEdit();
                frm.ShowDialog();
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("Please select an supplier to edit", "Notification", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            dataGridView1.DataSource = sup.getAll();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            dataGridView1.DataSource = sup.Search(textBox1.Text);
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }
    }
}
