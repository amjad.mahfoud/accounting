﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NewStyleAccounting.GUIL
{
    public partial class Fridges_Contents : Form
    {
        public Fridges_Contents()
        {
            InitializeComponent();
        }
        CLASSES.Fridge fr = new CLASSES.Fridge();
        private void Fridges_Contents_Load(object sender, EventArgs e)
        {
            int id = GUIL.Fridges.fId;
            dataGridView1.DataSource = fr.getContents(id);
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
