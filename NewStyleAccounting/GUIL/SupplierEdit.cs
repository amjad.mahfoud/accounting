﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace NewStyleAccounting.GUIL
{
    public partial class SupplierEdit : Form
    {
        public SupplierEdit()
        {
            InitializeComponent();
        }

        CLASSES.Suppliers sup = new CLASSES.Suppliers();

        private void SupplierEdit_Load(object sender, EventArgs e)
        {
            txtName.Text = GUIL.Supplier.EmpName;
            txtPhone.Focus();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("تأكيد تعديل مورد", "تعديل مورد", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                try
                {
                    sup.Update(txtName.Text, txtPhone.Text, txtMob.Text, txtAdd.Text);
                    MessageBox.Show("تم تعديل المرد بنجاح", "تعديل مورد", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (SqlException)
                {
                    MessageBox.Show("خظأ في البيانات", "تعديل مورد", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
                catch (Exception addExc)
                {
                    MessageBox.Show(addExc.Message, "تعديل مورد", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
            }
            this.Dispose();
        }
    }
}
