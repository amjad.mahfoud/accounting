﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NewStyleAccounting.GUIL
{
    public partial class HoleTradeOut : Form
    {
        public HoleTradeOut()
        {
            InitializeComponent();
        }
        CLASSES.SupTrad trader = new CLASSES.SupTrad();
        CLASSES.HoleTraderOut hto = new CLASSES.HoleTraderOut();

        private void HoleTradeOut_Load(object sender, EventArgs e)
        {
            cmbEmpName.DataSource = trader.getAll();
            cmbEmpName.DisplayMember = "الاسم";
            cmbEmpName.ValueMember = "الاسم";

            dataGridView1.DataSource = hto.getAll();
        }

        public static int FID = 0;
        public static double paid = 0;
        public static double total = 0;

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            dataGridView1.DataSource = hto.Search(textBox1.Text);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                hto.add(cmbEmpName.SelectedValue.ToString(), dateTimePicker1.Value);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            dataGridView1.DataSource = hto.getAll();
        }

        private void إضافةمكوناتToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                FID = Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value);
                paid = Convert.ToDouble(dataGridView1.CurrentRow.Cells[4].Value);
                total = Convert.ToDouble(dataGridView1.CurrentRow.Cells[2].Value);
                if (paid == total&&paid!=0)
                {
                    MessageBox.Show("لا يمكن إضافة مكونات", "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else
                {
                    new GUIL.OutContents().ShowDialog();
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            
            dataGridView1.DataSource = hto.getAll();
        }

        private void عرضالمكوناتToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                FID = Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value);
                new GUIL.ViewOutContents().ShowDialog();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void دفعمبلغToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                paid = Convert.ToDouble(dataGridView1.CurrentRow.Cells[4].Value);
                total = Convert.ToDouble(dataGridView1.CurrentRow.Cells[2].Value);
                if (paid == total)
                {
                    MessageBox.Show("الكمية مدفوعة بالفعل", "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else
                {
                    new GUIL.HoleTraderPay().ShowDialog();
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            dataGridView1.DataSource = hto.getAll();
        }
    }
}
