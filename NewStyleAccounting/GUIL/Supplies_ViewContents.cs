﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NewStyleAccounting.GUIL
{
    public partial class Supplies_ViewContents : Form
    {
        public Supplies_ViewContents()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Dispose();
        }
        CLASSES.Supplies sup = new CLASSES.Supplies();

        private void Supplies_ViewContents_Load(object sender, EventArgs e)
        {
            int SupId = GUIL.Supplies.SupId;
            dataGridView1.DataSource = sup.getAllContents(SupId);
        }

        private void dataGridView1_Click(object sender, EventArgs e)
        {
            Dispose();
        }
    }
}
