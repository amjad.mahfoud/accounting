﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace NewStyleAccounting.GUIL
{
    public partial class AddUser : Form
    {
        public AddUser()
        {
            InitializeComponent();
        }

        private void AddUser_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
        CLASSES.Users user = new CLASSES.Users();
        private void button1_Click(object sender, EventArgs e)
        {
            if (txtID.Text.Equals(string.Empty) || txtPWD.Text.Equals(string.Empty) || txtConfirm.Text.Equals(string.Empty))
            {
                MessageBox.Show("بعض الحقول الضرورية فارغة, الرجاء تعبئة جميع الحقول", "خطأ" ,MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                txtID.SelectionStart = 0;
                txtID.SelectionLength = txtID.TextLength;
                txtID.Focus();
                return;
            }
            if (!txtPWD.Text.Equals(txtConfirm.Text))
            {
                MessageBox.Show("تأكيد كلمة المرور خاطئ", "إضافة مستخدم", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtConfirm.SelectionStart = 0;
                txtConfirm.SelectionLength = txtID.TextLength;
                txtConfirm.Focus();
                return;
            }
            if (user.Exists(txtID.Text))
            {
                MessageBox.Show("المستخدم معرف مسبقا", "إضافة مستخدم", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtID.SelectionStart = 0;
                txtID.SelectionLength = txtID.TextLength;
                txtID.Focus();
            }
            else
            {
                user.addUser(txtID.Text, txtPWD.Text, "user");
                MessageBox.Show("تمت عملية الإضافة بنجاح", "إضافة مستخدم", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Form1.getMainForm.تسجيلالخروجToolStripMenuItem.Visible = true;
                Close();
            }
        }
    }
}
