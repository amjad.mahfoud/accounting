﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NewStyleAccounting.GUIL
{
    public partial class InContents : Form
    {
        public InContents()
        {
            InitializeComponent();
        }
        CLASSES.CInContents cic = new CLASSES.CInContents();
        private void InContents_Load(object sender, EventArgs e)
        {
            int id = GUIL.HoleTraderIn.FID;
            dataGridView1.DataSource = cic.get(id);
            textBox1.Text = id.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(textBox1.Text);
            try
            {
                cic.add(id, txtType.Text, Convert.ToDouble(txtQty.Text), Convert.ToDouble(txtUP.Text), Convert.ToDouble(textBox2.Text));
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            dataGridView1.DataSource = cic.get(id);
        }
    }
}
