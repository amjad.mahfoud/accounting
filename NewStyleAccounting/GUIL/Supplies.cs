﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NewStyleAccounting.GUIL
{
    public partial class Supplies : Form
    {
        CLASSES.Suppliers sup = new CLASSES.Suppliers();
        CLASSES.Supplies supplies = new CLASSES.Supplies();

        public Supplies()
        {
            InitializeComponent();
            cmbEmpName.DataSource = sup.getAll();
            cmbEmpName.DisplayMember = "الاسم";
            cmbEmpName.ValueMember = "الاسم";
        }

        public static int SupId;
        public static double SupPaid;
        public static double SupTotal;

        private void Supplies_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = supplies.getAll();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                supplies.Add(cmbEmpName.SelectedValue.ToString(), this.dateTimePicker1.Value);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            dataGridView1.DataSource = supplies.getAll();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            dataGridView1.DataSource = supplies.Search(textBox1.Text);
        }

        private void تشميعالكميةToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dataGridView1.CurrentRow.Cells[6].Value.ToString().Equals("True"))
            {
                MessageBox.Show("الكمية مشمعة بالفعل", "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                double aa = Convert.ToDouble(textBox2.Text);
                int id = Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value.ToString());
                supplies.WAX(id, aa);
            }
            dataGridView1.DataSource = supplies.getAll();
        }

        private void دفعمبلغToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataGridView1.CurrentRow.Cells[5].Value.ToString().Equals("True"))
                {
                    MessageBox.Show("الكمية مدفوعة بالفعل", "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else
                {
                    SupId = Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value.ToString());
                    SupPaid = Convert.ToDouble(dataGridView1.CurrentRow.Cells[3].Value.ToString());
                    SupTotal = Convert.ToDouble(dataGridView1.CurrentRow.Cells[4].Value.ToString());
                    new GUIL.SuppliesPay().ShowDialog();
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            dataGridView1.DataSource = supplies.getAll();
        }

        private void إضافةمكوناتToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dataGridView1.CurrentRow.Cells[5].Value.ToString().Equals("True"))
            {
                MessageBox.Show("الكمية مدفوعة لا يمكن اضافة مكونات", "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                SupId = Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value.ToString());
                new GUIL.Supplies_Contents().ShowDialog();
            }
            dataGridView1.DataSource = supplies.getAll();
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {

        }

        private void عرضالمحتوياتToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SupId = Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value.ToString());
            new GUIL.Supplies_ViewContents().ShowDialog();
        }

        private void طباعةالمكوناتToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                RPT.FRM_RPT frm = new RPT.FRM_RPT();
                RPT.rpt_goods_cont r = new RPT.rpt_goods_cont();
                r.SetDataSource(supplies.print(Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value.ToString())));
                frm.crystalReportViewer1.ReportSource = r;
                frm.ShowDialog();
            }
            catch (Exception)
            {
                MessageBox.Show("Please select a field in the table", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                return;
            }
        }
    }
}
