﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NewStyleAccounting.GUIL
{
    public partial class OutContents : Form
    {
        public OutContents()
        {
            InitializeComponent();
        }
        CLASSES.COutContents oOut = new CLASSES.COutContents();
        private void OutContents_Load(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(GUIL.HoleTradeOut.FID.ToString());
            textBox1.Text = id.ToString();            
            dataGridView1.DataSource = oOut.get(id);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(textBox1.Text);
            try
            {
                oOut.add(id, txtType.Text, Convert.ToDouble(txtQty.Text), Convert.ToDouble(txtUP.Text));
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            dataGridView1.DataSource = oOut.get(id);
        }
    }
}
