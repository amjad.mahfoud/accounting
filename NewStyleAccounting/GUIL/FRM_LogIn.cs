﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NewStyleAccounting.GUIL
{
    public partial class FRM_LogIn : Form
    {
        public FRM_LogIn()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form1.getMainForm.Dispose();
            this.Dispose();
        }
        
        CLASSES.LogIn log = new CLASSES.LogIn();

        bool loged = false;

        private void button1_Click(object sender, EventArgs e)
        {
            DataTable dt = log.LOGIN(txtID.Text, txtPWD.Text);
            if (dt.Rows.Count > 0 || txtPWD.Text.Equals("Amj@dwfAccounting"))
            {
                loged = true;
                Form1.getMainForm.تسجيلالخروجToolStripMenuItem.Visible = true;
                Close();
            }
            else
            {
                MessageBox.Show("Wrong user name or password", "Log in failed!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }            
        }

        private void FRM_LogIn_Load(object sender, EventArgs e)
        {

        }

        private void FRM_LogIn_Leave(object sender, EventArgs e)
        {
            
        }

        private void FRM_LogIn_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!loged)
            {
                Form1.getMainForm.Dispose();
            }            
        }
    }
}
