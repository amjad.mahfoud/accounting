﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace NewStyleAccounting.GUIL
{
    public partial class SupTrad : Form
    {
        public SupTrad()
        {
            InitializeComponent();
        }
        CLASSES.SupTrad trader = new CLASSES.SupTrad();
        private void button1_Click(object sender, EventArgs e)
        {
            if (txtName.Text.Equals(""))
            {
                MessageBox.Show("حقل الاسم فارغ", "حقول اساسية فارغة", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            if (MessageBox.Show("تأكيد إضافة تاجر", "إضافة تاجر", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                try
                {
                    trader.Add(txtName.Text, txtPhone.Text, txtMob.Text, txtAdd.Text);
                    MessageBox.Show("تم إضافة التاجر بنجاح", "إضافة تاجر", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtName.Text = "";
                    txtAdd.Text = "";
                    txtMob.Text = "";
                    txtPhone.Text = "";
                    txtName.Focus();
                }
                //catch (SqlException)
                //{
                //    MessageBox.Show("التاجر موجود مسبفا", "إضافة تاجر", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                //    txtName.Focus();
                //    txtName.SelectionStart = 0;
                //    txtName.SelectionLength = txtName.TextLength;
                //    return;
                //}
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message, "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
            }
            dataGridView1.DataSource = trader.getAll();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            dataGridView1.DataSource = trader.Search(textBox1.Text);
        }

        private void SupTrad_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = trader.getAll();
        }
        public static string name;

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                name = dataGridView1.CurrentRow.Cells[0].Value.ToString();
                new GUIL.SupTradUpdate().ShowDialog();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            this.dataGridView1.DataSource = trader.getAll();
        }
    }
}
