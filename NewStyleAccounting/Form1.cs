﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NewStyleAccounting
{
    public partial class Form1 : Form
    {

        private static Form1 frm;

        static void frm_FormClosed(object sender, FormClosedEventArgs e)
        {
            frm = null;
        }

        public static Form1 getMainForm
        {
            get
            {
                if (frm == null)
                {
                    frm = new Form1();
                    frm.FormClosed += new FormClosedEventHandler(frm_FormClosed);
                }
                return frm;
            }
        }

        public Form1()
        {
            InitializeComponent();
            if (frm == null)
            {
                frm = this;
            }
        }

        private void إغلاقToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you want to take a backup first", "Backup", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                da.BackupDB();
            }
            else
            {
                this.Dispose();
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.toolStripStatusLabel1.Text = DateTime.Now.ToString();
        }

        CLASSES.LogIn log = new CLASSES.LogIn();

        private void Form1_Load(object sender, EventArgs e)
        {
            // UNDONE 
            //Properties.Settings.Default.Registered = false;
            //Properties.Settings.Default.RegName = "";
            //Properties.Settings.Default.RegSerial = "";
            //Properties.Settings.Default.NumberOfRuns = 0;
            //Properties.Settings.Default.Save();
            //pictureBox1.BackColor = Color.SlateGray;
            ValidateProgram();

            this.toolStripStatusLabel1.Text = DateTime.Now.ToString();
            timer1.Start();

            if (log.HasUsers())
            {
                GUIL.FRM_LogIn logForm = new GUIL.FRM_LogIn();
                logForm.ShowDialog();
            }
            else
            {
                Form1.getMainForm.تسجيلالخروجToolStripMenuItem.Visible = false;
            }
        }

        public void ValidateProgram()
        {
            int numOfRuns = Properties.Settings.Default.NumberOfRuns;
            bool reg = Properties.Settings.Default.Registered;
            if (!reg)
            {
                if (numOfRuns >= 5)
                {
                    MessageBox.Show("انتهت الفترة التجريبية للمنتج, لمتابعة الإستخدام الرجاء القيام بتسجيل المنتج", "الرجاء تسجيل المنتج", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    new GUIL.RegApp().ShowDialog();
                }
                else
                {
                    Properties.Settings.Default.NumberOfRuns = numOfRuns + 1;
                    Properties.Settings.Default.Save();
                }
            }            
        }

        private void تسجيلالخروجToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new GUIL.FRM_LogIn().ShowDialog();
        }

        DBC.DataAccess da = new DBC.DataAccess();

        private void إنشاءنسخةToolStripMenuItem_Click(object sender, EventArgs e)
        {
            da.BackupDB();
        }

        private void إستعادةنسخةToolStripMenuItem_Click(object sender, EventArgs e)
        {
            da.RestoreDB();
        }

        private void مستخدمجديدToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GUIL.AddUser addUser = new GUIL.AddUser();
            addUser.ShowDialog();
        }

        private void إدارةالمستخدمينToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new GUIL.FRM_ManageUsers().ShowDialog();
        }

        private void حولHIGHTECHToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void حولالبرنامجToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void موظفجديدToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void إدارةالموظفينToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new GUIL.FRM_AddEmp().ShowDialog();
        }

        private void تسجيلالمنتجToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new GUIL.RegApp().ShowDialog();
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void حولHIGHTECHToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            new GUIL.AboutHightech().ShowDialog();
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            new RPT.FRM_RPT().ShowDialog();
        }

        private void حولالبرنامجToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            new AboutApp().ShowDialog();
        }

        private void دوامالموظفينToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new GUIL.FRM_EMP_WORK().ShowDialog();
        }

        private void toolStripMenuItem2_Click_1(object sender, EventArgs e)
        {
            
        }

        private void الرواتبToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new GUIL.FRM_Emp_Sal().ShowDialog();
        }

        private void السلفToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new GUIL.FRM_PrePayments().ShowDialog();
        }

        private void إدارةToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new GUIL.Supplier().ShowDialog();
        }

        private void الصناديقالحقليةToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new GUIL.FieldBoxes().ShowDialog();
        }

        private void الجمولاتToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new GUIL.Supplies().ShowDialog();
        }

        private void إدارةToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            new GUIL.Traders().ShowDialog();
        }

        private void البراداتToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void البراداتToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            new GUIL.Fridges().ShowDialog();
        }

        private void الحوالاتToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new GUIL.TranMony().ShowDialog();
        }

        private void مصاريفالشماعةToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new GUIL.Expences().ShowDialog();
        }

        private void تجارالجملةToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new GUIL.SupTrad().ShowDialog();
        }

        private void التشميعToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new GUIL.Waxed().ShowDialog();
        }

        private void تقاربرToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void إدارةToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            new GUIL.SupTrad().ShowDialog();
        }

        private void تصديرToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new GUIL.HoleTradeOut().ShowDialog();
        }

        private void الحساباتالجاريةToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new GUIL.OnGoingAccounts().ShowDialog();
        }

        private void توريدToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new GUIL.HoleTraderIn().ShowDialog();
        }

        private void حسبالتاريخToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new GUIL.Reports().ShowDialog();
        }

        CLASSES.CReports orep = new CLASSES.CReports();
        private void الربحالاجماليToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                RPT.FRM_RPT frm = new RPT.FRM_RPT();
                RPT.TP r = new RPT.TP();
                r.SetDataSource(orep.getTotalProfit());
                frm.crystalReportViewer1.ReportSource = r;
                frm.ShowDialog();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }
    }
}
