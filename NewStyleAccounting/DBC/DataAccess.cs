﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace NewStyleAccounting.DBC
{
    class DataAccess
    {
        SqlConnection sqlConnection;

        public DataAccess()
        {
            string server;
            server = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=|DataDirectory|\APP_DB.mdf; DataBase= APP_DB;Integrated Security=True";                       
            sqlConnection = new SqlConnection(server);            
        }

        public void Open()
        {
            if (sqlConnection.State != ConnectionState.Open)
            {
                sqlConnection.Open();
            }
        }

        public void Close()
        {
            if (sqlConnection.State == ConnectionState.Open)
            {
                sqlConnection.Close();
            }
        }

        public DataTable SelectData(string storedProcedure, SqlParameter[] param)
        {
            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlCmd.CommandText = storedProcedure;
            sqlCmd.Connection = sqlConnection;

            if (param != null)
            {
                sqlCmd.Parameters.AddRange(param);
            }
            SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }

        public void ExecuteCmd(string storedProcedure, SqlParameter[] param)
        {
            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlCmd.CommandText = storedProcedure;
            sqlCmd.Connection = sqlConnection;

            if (param != null)
            {
                sqlCmd.Parameters.AddRange(param);                
            }
            sqlCmd.ExecuteNonQuery();
        }

        public void BackupDB()
        {            
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.Filter = "HIGHTECH Backup File (*.htbk)|*.htbk";
            dlg.Title = "إنشاء نسخة";
            
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    Open();
                    string backupCMD = "";
                    backupCMD = @"BACKUP DATABASE APP_DB TO DISK='" + dlg.FileName + "' WITH INIT";
                    SqlCommand sqlCmd = new SqlCommand(backupCMD, sqlConnection);
                    sqlCmd.ExecuteNonQuery();
                    Close();
                    MessageBox.Show("تم إنشاء النسخة بنجاح", "إنشاء نسخة", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }                    
        }

        public void RestoreDB()
        {          
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "HIGHTECH Backup File (*.htbk)|*.htbk";
            dlg.Title = "إستعادة نسخة";
        
            if (dlg.ShowDialog() == DialogResult.OK)
            {                
                try
                {                    
                    Open();
                    string restoreCMD;                    
                    restoreCMD = @"use master; RESTORE DATABASE APP_DB FROM DISK = '" + dlg.FileName + "' WITH REPLACE";
                    SqlCommand sqlCmd = new SqlCommand(restoreCMD, sqlConnection);
                    sqlCmd.ExecuteNonQuery();
                    Close();
                    MessageBox.Show("تمت إستعادة النسخة بنجاح", "إستعادة نسخة", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }                        
        }
    }
}
