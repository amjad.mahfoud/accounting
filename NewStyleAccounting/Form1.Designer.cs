﻿namespace NewStyleAccounting
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.ملفToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.نسخإحتياطيToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.إنشاءنسخةToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.إستعادةنسخةToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.مصاريفالشماعةToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.التشميعToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.تسجيلالخروجToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.إغلاقToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.الموظفينToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.إدارةالموظفينToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.دوامالموظفينToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.الرواتبToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.السلفToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.الموردينToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.الصناديقالحقليةToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.إدارةToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.الجمولاتToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.التجارToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.إدارةToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.البراداتToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.تجارالجملةToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.إدارةToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.تصديرToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.الحساباتالجاريةToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.توريدToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.الحوالاتToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.تقاربرToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.حسبالتاريخToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.المستخدمينToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.مستخدمجديدToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.إدارةالمستخدمينToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.مساعدةToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.حولالبرنامجToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.حولHIGHTECHToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.تسجيلالمنتجToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.الربحالاجماليToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ملفToolStripMenuItem,
            this.الموظفينToolStripMenuItem,
            this.الموردينToolStripMenuItem,
            this.التجارToolStripMenuItem,
            this.تجارالجملةToolStripMenuItem,
            this.الحوالاتToolStripMenuItem,
            this.تقاربرToolStripMenuItem,
            this.المستخدمينToolStripMenuItem,
            this.مساعدةToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(922, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked);
            // 
            // ملفToolStripMenuItem
            // 
            this.ملفToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.نسخإحتياطيToolStripMenuItem,
            this.مصاريفالشماعةToolStripMenuItem,
            this.التشميعToolStripMenuItem,
            this.تسجيلالخروجToolStripMenuItem,
            this.toolStripSeparator1,
            this.إغلاقToolStripMenuItem});
            this.ملفToolStripMenuItem.Name = "ملفToolStripMenuItem";
            this.ملفToolStripMenuItem.Size = new System.Drawing.Size(42, 20);
            this.ملفToolStripMenuItem.Text = "ملف";
            // 
            // نسخإحتياطيToolStripMenuItem
            // 
            this.نسخإحتياطيToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.إنشاءنسخةToolStripMenuItem,
            this.إستعادةنسخةToolStripMenuItem});
            this.نسخإحتياطيToolStripMenuItem.Name = "نسخإحتياطيToolStripMenuItem";
            this.نسخإحتياطيToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.نسخإحتياطيToolStripMenuItem.Text = "نسخ إحتياطي";
            // 
            // إنشاءنسخةToolStripMenuItem
            // 
            this.إنشاءنسخةToolStripMenuItem.Name = "إنشاءنسخةToolStripMenuItem";
            this.إنشاءنسخةToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.إنشاءنسخةToolStripMenuItem.Text = "إنشاء نسخة";
            this.إنشاءنسخةToolStripMenuItem.Click += new System.EventHandler(this.إنشاءنسخةToolStripMenuItem_Click);
            // 
            // إستعادةنسخةToolStripMenuItem
            // 
            this.إستعادةنسخةToolStripMenuItem.Name = "إستعادةنسخةToolStripMenuItem";
            this.إستعادةنسخةToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.إستعادةنسخةToolStripMenuItem.Text = "إستعادة نسخة";
            this.إستعادةنسخةToolStripMenuItem.Click += new System.EventHandler(this.إستعادةنسخةToolStripMenuItem_Click);
            // 
            // مصاريفالشماعةToolStripMenuItem
            // 
            this.مصاريفالشماعةToolStripMenuItem.Name = "مصاريفالشماعةToolStripMenuItem";
            this.مصاريفالشماعةToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.مصاريفالشماعةToolStripMenuItem.Text = "مصاريف الشماعة";
            this.مصاريفالشماعةToolStripMenuItem.Click += new System.EventHandler(this.مصاريفالشماعةToolStripMenuItem_Click);
            // 
            // التشميعToolStripMenuItem
            // 
            this.التشميعToolStripMenuItem.Name = "التشميعToolStripMenuItem";
            this.التشميعToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.التشميعToolStripMenuItem.Text = "التشميع";
            this.التشميعToolStripMenuItem.Click += new System.EventHandler(this.التشميعToolStripMenuItem_Click);
            // 
            // تسجيلالخروجToolStripMenuItem
            // 
            this.تسجيلالخروجToolStripMenuItem.Name = "تسجيلالخروجToolStripMenuItem";
            this.تسجيلالخروجToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.تسجيلالخروجToolStripMenuItem.Text = "تسجيل الخروج";
            this.تسجيلالخروجToolStripMenuItem.Click += new System.EventHandler(this.تسجيلالخروجToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(158, 6);
            // 
            // إغلاقToolStripMenuItem
            // 
            this.إغلاقToolStripMenuItem.Name = "إغلاقToolStripMenuItem";
            this.إغلاقToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.إغلاقToolStripMenuItem.Text = "إغلاق";
            this.إغلاقToolStripMenuItem.Click += new System.EventHandler(this.إغلاقToolStripMenuItem_Click);
            // 
            // الموظفينToolStripMenuItem
            // 
            this.الموظفينToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.إدارةالموظفينToolStripMenuItem,
            this.دوامالموظفينToolStripMenuItem,
            this.الرواتبToolStripMenuItem,
            this.السلفToolStripMenuItem});
            this.الموظفينToolStripMenuItem.Name = "الموظفينToolStripMenuItem";
            this.الموظفينToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
            this.الموظفينToolStripMenuItem.Text = "الموظفين";
            // 
            // إدارةالموظفينToolStripMenuItem
            // 
            this.إدارةالموظفينToolStripMenuItem.Name = "إدارةالموظفينToolStripMenuItem";
            this.إدارةالموظفينToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.إدارةالموظفينToolStripMenuItem.Text = "إدارة الموظفين";
            this.إدارةالموظفينToolStripMenuItem.Click += new System.EventHandler(this.إدارةالموظفينToolStripMenuItem_Click);
            // 
            // دوامالموظفينToolStripMenuItem
            // 
            this.دوامالموظفينToolStripMenuItem.Name = "دوامالموظفينToolStripMenuItem";
            this.دوامالموظفينToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.دوامالموظفينToolStripMenuItem.Text = "دوام الموظفين";
            this.دوامالموظفينToolStripMenuItem.Click += new System.EventHandler(this.دوامالموظفينToolStripMenuItem_Click);
            // 
            // الرواتبToolStripMenuItem
            // 
            this.الرواتبToolStripMenuItem.Name = "الرواتبToolStripMenuItem";
            this.الرواتبToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.الرواتبToolStripMenuItem.Text = "الرواتب";
            this.الرواتبToolStripMenuItem.Click += new System.EventHandler(this.الرواتبToolStripMenuItem_Click);
            // 
            // السلفToolStripMenuItem
            // 
            this.السلفToolStripMenuItem.Name = "السلفToolStripMenuItem";
            this.السلفToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.السلفToolStripMenuItem.Text = "السلف";
            this.السلفToolStripMenuItem.Click += new System.EventHandler(this.السلفToolStripMenuItem_Click);
            // 
            // الموردينToolStripMenuItem
            // 
            this.الموردينToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.الصناديقالحقليةToolStripMenuItem,
            this.إدارةToolStripMenuItem,
            this.الجمولاتToolStripMenuItem});
            this.الموردينToolStripMenuItem.Name = "الموردينToolStripMenuItem";
            this.الموردينToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.الموردينToolStripMenuItem.Text = "الموردين";
            // 
            // الصناديقالحقليةToolStripMenuItem
            // 
            this.الصناديقالحقليةToolStripMenuItem.Name = "الصناديقالحقليةToolStripMenuItem";
            this.الصناديقالحقليةToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.الصناديقالحقليةToolStripMenuItem.Text = "الصناديق الحقلية";
            this.الصناديقالحقليةToolStripMenuItem.Click += new System.EventHandler(this.الصناديقالحقليةToolStripMenuItem_Click);
            // 
            // إدارةToolStripMenuItem
            // 
            this.إدارةToolStripMenuItem.Name = "إدارةToolStripMenuItem";
            this.إدارةToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.إدارةToolStripMenuItem.Text = "إدارة الموردين";
            this.إدارةToolStripMenuItem.Click += new System.EventHandler(this.إدارةToolStripMenuItem_Click);
            // 
            // الجمولاتToolStripMenuItem
            // 
            this.الجمولاتToolStripMenuItem.Name = "الجمولاتToolStripMenuItem";
            this.الجمولاتToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.الجمولاتToolStripMenuItem.Text = "الحمولات";
            this.الجمولاتToolStripMenuItem.Click += new System.EventHandler(this.الجمولاتToolStripMenuItem_Click);
            // 
            // التجارToolStripMenuItem
            // 
            this.التجارToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.إدارةToolStripMenuItem1,
            this.البراداتToolStripMenuItem});
            this.التجارToolStripMenuItem.Name = "التجارToolStripMenuItem";
            this.التجارToolStripMenuItem.Size = new System.Drawing.Size(45, 20);
            this.التجارToolStripMenuItem.Text = "التجار";
            // 
            // إدارةToolStripMenuItem1
            // 
            this.إدارةToolStripMenuItem1.Name = "إدارةToolStripMenuItem1";
            this.إدارةToolStripMenuItem1.Size = new System.Drawing.Size(125, 22);
            this.إدارةToolStripMenuItem1.Text = "إدارة التجار";
            this.إدارةToolStripMenuItem1.Click += new System.EventHandler(this.إدارةToolStripMenuItem1_Click);
            // 
            // البراداتToolStripMenuItem
            // 
            this.البراداتToolStripMenuItem.Name = "البراداتToolStripMenuItem";
            this.البراداتToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.البراداتToolStripMenuItem.Text = "البرادات";
            this.البراداتToolStripMenuItem.Click += new System.EventHandler(this.البراداتToolStripMenuItem_Click_1);
            // 
            // تجارالجملةToolStripMenuItem
            // 
            this.تجارالجملةToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.إدارةToolStripMenuItem2,
            this.تصديرToolStripMenuItem,
            this.الحساباتالجاريةToolStripMenuItem,
            this.توريدToolStripMenuItem});
            this.تجارالجملةToolStripMenuItem.Name = "تجارالجملةToolStripMenuItem";
            this.تجارالجملةToolStripMenuItem.Size = new System.Drawing.Size(72, 20);
            this.تجارالجملةToolStripMenuItem.Text = "تجار الجملة";
            // 
            // إدارةToolStripMenuItem2
            // 
            this.إدارةToolStripMenuItem2.Name = "إدارةToolStripMenuItem2";
            this.إدارةToolStripMenuItem2.Size = new System.Drawing.Size(155, 22);
            this.إدارةToolStripMenuItem2.Text = "إدارة";
            this.إدارةToolStripMenuItem2.Click += new System.EventHandler(this.إدارةToolStripMenuItem2_Click);
            // 
            // تصديرToolStripMenuItem
            // 
            this.تصديرToolStripMenuItem.Name = "تصديرToolStripMenuItem";
            this.تصديرToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.تصديرToolStripMenuItem.Text = "تصدير";
            this.تصديرToolStripMenuItem.Click += new System.EventHandler(this.تصديرToolStripMenuItem_Click);
            // 
            // الحساباتالجاريةToolStripMenuItem
            // 
            this.الحساباتالجاريةToolStripMenuItem.Name = "الحساباتالجاريةToolStripMenuItem";
            this.الحساباتالجاريةToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.الحساباتالجاريةToolStripMenuItem.Text = "الحسابات الجارية";
            this.الحساباتالجاريةToolStripMenuItem.Click += new System.EventHandler(this.الحساباتالجاريةToolStripMenuItem_Click);
            // 
            // توريدToolStripMenuItem
            // 
            this.توريدToolStripMenuItem.Name = "توريدToolStripMenuItem";
            this.توريدToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.توريدToolStripMenuItem.Text = "توريد";
            this.توريدToolStripMenuItem.Click += new System.EventHandler(this.توريدToolStripMenuItem_Click);
            // 
            // الحوالاتToolStripMenuItem
            // 
            this.الحوالاتToolStripMenuItem.Name = "الحوالاتToolStripMenuItem";
            this.الحوالاتToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.الحوالاتToolStripMenuItem.Text = "الحوالات";
            this.الحوالاتToolStripMenuItem.Click += new System.EventHandler(this.الحوالاتToolStripMenuItem_Click);
            // 
            // تقاربرToolStripMenuItem
            // 
            this.تقاربرToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.حسبالتاريخToolStripMenuItem,
            this.الربحالاجماليToolStripMenuItem});
            this.تقاربرToolStripMenuItem.Name = "تقاربرToolStripMenuItem";
            this.تقاربرToolStripMenuItem.Size = new System.Drawing.Size(46, 20);
            this.تقاربرToolStripMenuItem.Text = "تقاربر";
            this.تقاربرToolStripMenuItem.Click += new System.EventHandler(this.تقاربرToolStripMenuItem_Click);
            // 
            // حسبالتاريخToolStripMenuItem
            // 
            this.حسبالتاريخToolStripMenuItem.Name = "حسبالتاريخToolStripMenuItem";
            this.حسبالتاريخToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.حسبالتاريخToolStripMenuItem.Text = "حسب التاريخ";
            this.حسبالتاريخToolStripMenuItem.Click += new System.EventHandler(this.حسبالتاريخToolStripMenuItem_Click);
            // 
            // المستخدمينToolStripMenuItem
            // 
            this.المستخدمينToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.مستخدمجديدToolStripMenuItem,
            this.إدارةالمستخدمينToolStripMenuItem});
            this.المستخدمينToolStripMenuItem.Name = "المستخدمينToolStripMenuItem";
            this.المستخدمينToolStripMenuItem.Size = new System.Drawing.Size(79, 20);
            this.المستخدمينToolStripMenuItem.Text = "المستخدمين";
            // 
            // مستخدمجديدToolStripMenuItem
            // 
            this.مستخدمجديدToolStripMenuItem.Name = "مستخدمجديدToolStripMenuItem";
            this.مستخدمجديدToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.مستخدمجديدToolStripMenuItem.Text = "مستخدم جديد";
            this.مستخدمجديدToolStripMenuItem.Click += new System.EventHandler(this.مستخدمجديدToolStripMenuItem_Click);
            // 
            // إدارةالمستخدمينToolStripMenuItem
            // 
            this.إدارةالمستخدمينToolStripMenuItem.Name = "إدارةالمستخدمينToolStripMenuItem";
            this.إدارةالمستخدمينToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.إدارةالمستخدمينToolStripMenuItem.Text = "إدارة المستخدمين";
            this.إدارةالمستخدمينToolStripMenuItem.Click += new System.EventHandler(this.إدارةالمستخدمينToolStripMenuItem_Click);
            // 
            // مساعدةToolStripMenuItem
            // 
            this.مساعدةToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.حولالبرنامجToolStripMenuItem,
            this.حولHIGHTECHToolStripMenuItem,
            this.تسجيلالمنتجToolStripMenuItem});
            this.مساعدةToolStripMenuItem.Name = "مساعدةToolStripMenuItem";
            this.مساعدةToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.مساعدةToolStripMenuItem.Text = "مساعدة";
            // 
            // حولالبرنامجToolStripMenuItem
            // 
            this.حولالبرنامجToolStripMenuItem.Name = "حولالبرنامجToolStripMenuItem";
            this.حولالبرنامجToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.حولالبرنامجToolStripMenuItem.Text = "حول البرنامج";
            this.حولالبرنامجToolStripMenuItem.Click += new System.EventHandler(this.حولالبرنامجToolStripMenuItem_Click_1);
            // 
            // حولHIGHTECHToolStripMenuItem
            // 
            this.حولHIGHTECHToolStripMenuItem.Image = global::NewStyleAccounting.Properties.Resources.Grey_Info;
            this.حولHIGHTECHToolStripMenuItem.Name = "حولHIGHTECHToolStripMenuItem";
            this.حولHIGHTECHToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.حولHIGHTECHToolStripMenuItem.Text = "حول HIGHTECH";
            this.حولHIGHTECHToolStripMenuItem.Click += new System.EventHandler(this.حولHIGHTECHToolStripMenuItem_Click_1);
            // 
            // تسجيلالمنتجToolStripMenuItem
            // 
            this.تسجيلالمنتجToolStripMenuItem.Name = "تسجيلالمنتجToolStripMenuItem";
            this.تسجيلالمنتجToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.تسجيلالمنتجToolStripMenuItem.Text = "تسجيل المنتج";
            this.تسجيلالمنتجToolStripMenuItem.Click += new System.EventHandler(this.تسجيلالمنتجToolStripMenuItem_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.BackColor = System.Drawing.SystemColors.Control;
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 421);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(922, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(48, 17);
            this.toolStripStatusLabel1.Text = "tsStatus";
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.PowderBlue;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Image = global::NewStyleAccounting.Properties.Resources._12;
            this.pictureBox1.Location = new System.Drawing.Point(0, 24);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(922, 397);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // الربحالاجماليToolStripMenuItem
            // 
            this.الربحالاجماليToolStripMenuItem.Name = "الربحالاجماليToolStripMenuItem";
            this.الربحالاجماليToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.الربحالاجماليToolStripMenuItem.Text = "الربح الاجمالي";
            this.الربحالاجماليToolStripMenuItem.Click += new System.EventHandler(this.الربحالاجماليToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DimGray;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(922, 443);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "محاسبي";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem ملفToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem نسخإحتياطيToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem إنشاءنسخةToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem إستعادةنسخةToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem إغلاقToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem الموظفينToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem الموردينToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem التجارToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem الحوالاتToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem المستخدمينToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem مساعدةToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem مستخدمجديدToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem إدارةالمستخدمينToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem حولالبرنامجToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem حولHIGHTECHToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem تسجيلالمنتجToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem تسجيلالخروجToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem إدارةالموظفينToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem دوامالموظفينToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem الرواتبToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem السلفToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem الصناديقالحقليةToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ToolStripMenuItem مصاريفالشماعةToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem إدارةToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem الجمولاتToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem إدارةToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem البراداتToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem تقاربرToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem التشميعToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem تجارالجملةToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem إدارةToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem تصديرToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem الحساباتالجاريةToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem توريدToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem حسبالتاريخToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem الربحالاجماليToolStripMenuItem;
    }
}

