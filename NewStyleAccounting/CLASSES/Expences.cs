﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace NewStyleAccounting.CLASSES
{
    class Expences
    {
        public void addEmp(string type, string notes, double amm, DateTime d)
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();
            SqlParameter[] param = new SqlParameter[4];

            param[0] = new SqlParameter("@Etype", SqlDbType.NVarChar, 50);
            param[0].Value = type;

            param[1] = new SqlParameter("@Notes", SqlDbType.NVarChar, 250);
            param[1].Value = notes;

            param[2] = new SqlParameter("@EAmm", SqlDbType.Real);
            param[2].Value = amm;

            param[3] = new SqlParameter("@Eday", SqlDbType.Date);
            param[3].Value = d;

            DA.ExecuteCmd("Expences_ADD", param);
            DA.Close();
        }

        public void del(int id)
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();
            SqlParameter[] param = new SqlParameter[1];

            param[0] = new SqlParameter("@id", SqlDbType.Int);
            param[0].Value = id;

            DA.ExecuteCmd("Expences_Del", param);
            DA.Close();
        }

        public DataTable getAll()
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();

            DataTable dt = new DataTable();
            dt = DA.SelectData("Expences_Get", null);

            DA.Close();

            return dt;
        }
       
    }
}
