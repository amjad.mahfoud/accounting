﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace NewStyleAccounting.CLASSES
{
    class Employee
    {
        public void addEmp(string Name, string Phone, string Mobile, string Add, long Sal, DateTime d)
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();
            SqlParameter[] param = new SqlParameter[6];

            param[0] = new SqlParameter("@Name", SqlDbType.NVarChar, 50);
            param[0].Value = Name;

            param[1] = new SqlParameter("@Phone", SqlDbType.NVarChar, 50);
            param[1].Value = Phone;

            param[2] = new SqlParameter("@Mobile", SqlDbType.NVarChar, 50);
            param[2].Value = Mobile;

            param[3] = new SqlParameter("@Add", SqlDbType.NVarChar, 50);
            param[3].Value = Add;

            param[4] = new SqlParameter("@Sal", SqlDbType.BigInt);
            param[4].Value = Sal;



            param[5] = new SqlParameter("@EDay", SqlDbType.Date);
            param[5].Value = d;

            DA.ExecuteCmd("AddEmp", param);
            DA.Close();
        }
       
        public void UpdateEmp(string Name, string Phone, string Mobile, string Add, long Sal)
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();
            SqlParameter[] param = new SqlParameter[5];

            param[0] = new SqlParameter("@Name", SqlDbType.NVarChar, 50);
            param[0].Value = Name;

            param[1] = new SqlParameter("@Phone", SqlDbType.NVarChar, 50);
            param[1].Value = Phone;

            param[2] = new SqlParameter("@Mobile", SqlDbType.NVarChar, 50);
            param[2].Value = Mobile;

            param[3] = new SqlParameter("@Add", SqlDbType.NVarChar, 50);
            param[3].Value = Add;

            param[4] = new SqlParameter("@Sal", SqlDbType.BigInt);
            param[4].Value = Sal;


            DA.ExecuteCmd("UpdateEmp", param);
            DA.Close();
        }

        public DataTable getAllEmp()
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();

            DataTable dt = new DataTable();
            dt = DA.SelectData("GetEmp", null);

            DA.Close();

            return dt;
        }
         
        public DataTable getAllPrePayments()
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();

            DataTable dt = new DataTable();
            dt = DA.SelectData("GetAllPrePayments", null);

            DA.Close();

            return dt;
        }
        public DataTable getSalaries()
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();

            DataTable dt = new DataTable();
            dt = DA.SelectData("SelectSal", null);

            DA.Close();

            return dt;
        }

        public DataTable getSalaries(string Name)
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();
            SqlParameter[] param = new SqlParameter[1];

            param[0] = new SqlParameter("@SName", SqlDbType.NVarChar, 50);
            param[0].Value = Name;

            DataTable dt = new DataTable();
            dt = DA.SelectData("SelectSalByName_Date", param);

            DA.Close();

            return dt;
        }

        public DataTable getWorkData()
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();

            DataTable dt = new DataTable();
            dt = DA.SelectData("GetWorkDate", null);

            DA.Close();

            return dt;
        }

        public DataTable getWorkData(string Name)
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();
            SqlParameter[] param = new SqlParameter[1];

            param[0] = new SqlParameter("@Name", SqlDbType.NVarChar, 50);
            param[0].Value = Name;

            DataTable dt = new DataTable();
            dt = DA.SelectData("GetWorkDataForUser", param);

            DA.Close();

            return dt;
        }
        
        public DataTable getWorkData(string Name , DateTime Month)
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();
            SqlParameter[] param = new SqlParameter[2];

            param[0] = new SqlParameter("@Name", SqlDbType.NVarChar, 50);
            param[0].Value = Name;

            param[1] = new SqlParameter("@WorkMonth", SqlDbType.Date);
            param[1].Value = Month;

            DataTable dt = new DataTable();
            dt = DA.SelectData("GetWorkDataForUserByDate", param);

            DA.Close();

            return dt;
        }

        public DataTable getEmpSal(string Name)
        {
            DBC.DataAccess DA = new DBC.DataAccess();
            SqlParameter[] param = new SqlParameter[1];

            param[0] = new SqlParameter("@Name", SqlDbType.NVarChar, 50);
            param[0].Value = Name;

            DataTable dt = new DataTable();
            dt = DA.SelectData("GET_EMP_SAL", param);

            DA.Close();            
            return dt;
        }

        public void addWorkDay(string Name, int ExtraHrs, double DaySal, bool FullDay, DateTime d)
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();
            SqlParameter[] param = new SqlParameter[5];

            param[0] = new SqlParameter("@EmpName", SqlDbType.NVarChar, 50);
            param[0].Value = Name;

            param[1] = new SqlParameter("@ExtraHrs", SqlDbType.Int);
            param[1].Value = ExtraHrs;

            param[2] = new SqlParameter("@DaySal", SqlDbType.Real);
            param[2].Value = DaySal;

            param[3] = new SqlParameter("@FullDay", SqlDbType.Bit);
            param[3].Value = FullDay;

            param[4] = new SqlParameter("@WDay", SqlDbType.Date);
            param[4].Value = d;

            DA.ExecuteCmd("AddWorkDay", param);
            DA.Close();
        }
        public void AddPrePayment(string Name, double Amm, DateTime d)
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();
            SqlParameter[] param = new SqlParameter[3];

            param[0] = new SqlParameter("@EName", SqlDbType.NVarChar, 50);
            param[0].Value = Name;

            param[1] = new SqlParameter("@Amm", SqlDbType.Real);
            param[1].Value = Amm;

            param[2] = new SqlParameter("@PDay", SqlDbType.Date);
            param[2].Value = d;

            DA.ExecuteCmd("AddPrePayment", param);
            DA.Close();
        }

        public void ReturnPrePayment(int Id)
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();
            SqlParameter[] param = new SqlParameter[1];

            param[0] = new SqlParameter("@Id", SqlDbType.Int);
            param[0].Value = Id;

            DA.ExecuteCmd("ReturnPrePayment", param);
            DA.Close();
        }

        public void PaySal(int Id)
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();
            SqlParameter[] param = new SqlParameter[1];

            param[0] = new SqlParameter("@Id", SqlDbType.NVarChar, 50);
            param[0].Value = Id;

            DA.ExecuteCmd("PaySal", param);
            DA.Close();
        }

        
        //public void DeleteEMP(string ID)
        //{
        //    DBC.DataAccess DA = new DBC.DataAccess();
        //    SqlParameter[] param = new SqlParameter[1];

        //    param[0] = new SqlParameter("@ID", SqlDbType.NVarChar, 50);
        //    param[0].Value = ID;
        //    DA.Open();
        //    DA.ExecuteCmd("DeleteEmp", param);
        //    DA.Close();
        //}
    }
}
