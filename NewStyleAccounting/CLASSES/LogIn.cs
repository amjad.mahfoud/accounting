﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace NewStyleAccounting.CLASSES
{
    class LogIn
    {
        public DataTable LOGIN(string ID, string PWD)
        {
            DBC.DataAccess DA = new DBC.DataAccess();
            SqlParameter[] param = new SqlParameter[2];

            param[0] = new SqlParameter("@ID", SqlDbType.NVarChar, 50);
            param[0].Value = ID;

            param[1] = new SqlParameter("@PWD", SqlDbType.NVarChar, 50);
            param[1].Value = PWD;

            DA.Open();

            DataTable dt = new DataTable();
            dt = DA.SelectData("VerifyUser", param);

            DA.Close();

            return dt;
        }

        public bool HasUsers()
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();

            DataTable dt = new DataTable();
            dt = DA.SelectData("HasUsers", null);

            DA.Close();

            if (dt.Rows.Count > 0)
            {
                return true;
            }
            return false;
        }
    }
}
