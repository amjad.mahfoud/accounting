﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace NewStyleAccounting.CLASSES
{
    class FieldBoxes
    {
        public DataTable getAll()
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();

            DataTable dt = new DataTable();
            dt = DA.SelectData("FieldBoxes_GetAll", null);

            DA.Close();

            return dt;
        }

        public DataTable Search(string Name)
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();
            SqlParameter[] param = new SqlParameter[1];

            param[0] = new SqlParameter("@id", SqlDbType.NVarChar, 50);
            param[0].Value = Name;

            DataTable dt = new DataTable();
            dt = DA.SelectData("FieldBoxes_Search", param);

            DA.Close();

            return dt;
        }

        public void Add(string Name, int Num, DateTime d)
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();
            SqlParameter[] param = new SqlParameter[3];

            param[0] = new SqlParameter("@Sname", SqlDbType.NVarChar, 50);
            param[0].Value = Name;

            param[1] = new SqlParameter("@NumGivin", SqlDbType.Int);
            param[1].Value = Num;

            param[2] = new SqlParameter("@OpDate", SqlDbType.Date);
            param[2].Value = d;

            DA.ExecuteCmd("FieldBoxes_Add", param);
            DA.Close();
        }



        public void Return(string Name, int Num, DateTime d)
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();
            SqlParameter[] param = new SqlParameter[3];

            param[0] = new SqlParameter("@Sname", SqlDbType.NVarChar, 50);
            param[0].Value = Name;

            param[1] = new SqlParameter("@NumReturned", SqlDbType.Int);
            param[1].Value = Num;

            param[2] = new SqlParameter("@OpDate", SqlDbType.Date);
            param[2].Value = d;

            DA.ExecuteCmd("FieldBoxes_Return", param);
            DA.Close();
        }
    }
}
