﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace NewStyleAccounting.CLASSES
{
    class HoleTraderIn
    {
        public void add(string Name, DateTime d)
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();
            SqlParameter[] param = new SqlParameter[2];

            param[0] = new SqlParameter("@TName", SqlDbType.NVarChar, 50);
            param[0].Value = Name;

            param[1] = new SqlParameter("@OpDate", SqlDbType.Date);
            param[1].Value = d;

            DA.ExecuteCmd("HoleTrader_In_Add", param);
            DA.Close();
        }

        public void wax(int id, double waxPer)
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();
            SqlParameter[] param = new SqlParameter[2];

            param[0] = new SqlParameter("@Id", SqlDbType.Int);
            param[0].Value = id;

            param[1] = new SqlParameter("@WaxPer", SqlDbType.Real);
            param[1].Value = waxPer;

            DA.ExecuteCmd("InWax", param);
            DA.Close();
        }

        public DataTable getAll()
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();

            DataTable dt = new DataTable();
            dt = DA.SelectData("HoleTrader_In_GetAll", null);

            DA.Close();

            return dt;
        }

        public DataTable Search(string Name)
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();
            SqlParameter[] param = new SqlParameter[1];

            param[0] = new SqlParameter("@TName", SqlDbType.NVarChar, 50);
            param[0].Value = Name;

            DataTable dt = new DataTable();
            dt = DA.SelectData("HoleTrader_In_Search", param);

            DA.Close();

            return dt;
        }
    }
}
