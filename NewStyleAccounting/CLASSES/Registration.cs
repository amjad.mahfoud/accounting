﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HighTech;

namespace NewStyleAccounting.CLASSES
{
    class Registration
    {
        void commit()
        {
            Properties.Settings.Default.Save();
        }
        public bool Registered
        {
            get
            {
                return Properties.Settings.Default.Registered;
            }
            set
            {
                Properties.Settings.Default.Registered = value;
                commit();
            }
        }

        public string DeviceID
        {
            get 
            {
                return Properties.Settings.Default.DeviceID;
            }
            set 
            {
                Properties.Settings.Default.DeviceID = value;
                commit();
            }
        }

        public int NumberOfRuns
        {
            get
            {
                return Properties.Settings.Default.NumberOfRuns;
            }
            set
            {
                Properties.Settings.Default.NumberOfRuns = value;
                commit();
            }
        }
    }
}
