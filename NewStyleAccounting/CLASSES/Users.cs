﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;


namespace NewStyleAccounting.CLASSES
{
    class Users
    {
        public void addUser(string ID, string PWD, string RO)
        {

            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();
            SqlParameter[] param = new SqlParameter[3];

            param[0] = new SqlParameter("@ID", SqlDbType.NVarChar, 50);
            param[0].Value = ID;

            param[1] = new SqlParameter("@PWD", SqlDbType.NVarChar, 50);
            param[1].Value = PWD;

            param[2] = new SqlParameter("@USERROLE", SqlDbType.NChar, 10);
            param[2].Value = RO;

            
            DA.ExecuteCmd("AddUser", param);
            DA.Close();
        }

        public bool Exists(string ID)
        {
            DBC.DataAccess DA = new DBC.DataAccess();
            SqlParameter[] param = new SqlParameter[1];

            param[0] = new SqlParameter("@ID", SqlDbType.NVarChar, 50);
            param[0].Value = ID;
            
            DA.Open();

            DataTable dt = new DataTable();
            dt = DA.SelectData("Exists", param);

            DA.Close();
            if (dt.Rows.Count > 0)
            {
                return true;
            }
            return false;
        }

        public DataTable getAllUsers()
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();

            DataTable dt = new DataTable();
            dt = DA.SelectData("GetUsers", null);

            DA.Close();

            return dt;
        }

        public DataTable SearchUsers(string ID)
        {
            DBC.DataAccess DA = new DBC.DataAccess();
            SqlParameter[] param = new SqlParameter[1];

            param[0] = new SqlParameter("@ID", SqlDbType.NVarChar, 50);
            param[0].Value = ID;
            DA.Open();

            DataTable dt = new DataTable();
            dt = DA.SelectData("SearchUsers", param);

            DA.Close();

            return dt;
        }

        public void DeleteUser(string ID)
        {
            DBC.DataAccess DA = new DBC.DataAccess();
            SqlParameter[] param = new SqlParameter[1];

            param[0] = new SqlParameter("@ID", SqlDbType.NVarChar, 50);
            param[0].Value = ID;
            DA.Open();            
            DA.ExecuteCmd("DeleteUser", param);
            DA.Close();
        }
    }
}
