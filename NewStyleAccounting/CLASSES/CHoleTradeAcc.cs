﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace NewStyleAccounting.CLASSES
{
    class CHoleTradeAcc
    {
        public DataTable get()
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();

            DataTable dt = new DataTable();

            dt = DA.SelectData("HoleTrade_Accounting_GetAll", null);

            DA.Close();

            return dt;
        }
    }
}
