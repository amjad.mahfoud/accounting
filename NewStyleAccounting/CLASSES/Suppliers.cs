﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace NewStyleAccounting.CLASSES
{
    class Suppliers
    {
        public void Add(string Name, string Phone, string Mobile,string SupAdd)
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();
            SqlParameter[] param = new SqlParameter[4];

            param[0] = new SqlParameter("@Name", SqlDbType.NVarChar, 50);
            param[0].Value = Name;

            param[1] = new SqlParameter("@Phone", SqlDbType.NVarChar, 50);
            param[1].Value = Phone;

            param[2] = new SqlParameter("@Mobile", SqlDbType.NVarChar, 50);
            param[2].Value = Mobile;
            
            param[3] = new SqlParameter("@SupAdd", SqlDbType.NVarChar, 50);
            param[3].Value = SupAdd;
            
            DA.ExecuteCmd("Suppliers_Add", param);
            DA.Close();
        }

        
        public DataTable getAll()
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();

            DataTable dt = new DataTable();
            dt = DA.SelectData("Suppliers_Get_All", null);

            DA.Close();

            return dt;
        }        

        public void Update(string Name, string Phone, string Mobile, string Add)
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();
            SqlParameter[] param = new SqlParameter[4];

            param[0] = new SqlParameter("@Name", SqlDbType.NVarChar, 50);
            param[0].Value = Name;

            param[1] = new SqlParameter("@Phone", SqlDbType.NVarChar, 50);
            param[1].Value = Phone;

            param[2] = new SqlParameter("@Mobile", SqlDbType.NVarChar, 50);
            param[2].Value = Mobile;

            param[3] = new SqlParameter("@SupAdd", SqlDbType.NVarChar, 50);
            param[3].Value = Add;
            
            DA.ExecuteCmd("Supplier_Update", param);
            DA.Close();
        }

        public DataTable Search(string Name)
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();
            SqlParameter[] param = new SqlParameter[1];

            param[0] = new SqlParameter("@Name", SqlDbType.NVarChar, 50);
            param[0].Value = Name;

            DataTable dt = new DataTable();
            dt = DA.SelectData("Suppliers_Search", param);

            DA.Close();

            return dt;
        }
    }
}
