﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace NewStyleAccounting.CLASSES
{
    class COutContents
    {
        public void add(int gid, string ctype, double weight, double unitPrice)
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();
            SqlParameter[] param = new SqlParameter[4];

            param[0] = new SqlParameter("@Gid", SqlDbType.Int);
            param[0].Value = gid;

            param[1] = new SqlParameter("@Ctype", SqlDbType.NVarChar, 50);
            param[1].Value = ctype;

            param[2] = new SqlParameter("@CWeight", SqlDbType.Real);
            param[2].Value = weight;

            param[3] = new SqlParameter("@UnitPrice", SqlDbType.Real);
            param[3].Value = unitPrice;

            DA.ExecuteCmd("OutContents_Add", param);
            DA.Close();
        }

        public DataTable get(int id)
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();

            DataTable dt = new DataTable();

            SqlParameter[] param = new SqlParameter[1];

            param[0] = new SqlParameter("@Gid", SqlDbType.Int);
            param[0].Value = id;

            dt = DA.SelectData("OutContents_Get", param);

            DA.Close();

            return dt;
        }
    }
}
