﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
namespace NewStyleAccounting.CLASSES
{
    class Fridge
    {
        public void Add(string Name, string truckID, string Dname, string p1, string p2, double tranCoast, DateTime d)
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();
            SqlParameter[] param = new SqlParameter[8];

            param[0] = new SqlParameter("@TraderName", SqlDbType.NVarChar, 50);
            param[0].Value = Name;

            param[1] = new SqlParameter("@Coast", SqlDbType.Real);
            param[1].Value = 0;

            param[2] = new SqlParameter("@TruckID", SqlDbType.NVarChar, 50);
            param[2].Value = truckID;

            param[3] = new SqlParameter("@DriverName", SqlDbType.NVarChar, 50);
            param[3].Value = Dname;

            param[4] = new SqlParameter("@DriverPhone", SqlDbType.NVarChar, 50);
            param[4].Value = p1;

            param[5] = new SqlParameter("@DriverMobile", SqlDbType.NVarChar, 50);
            param[5].Value = p2;

            param[6] = new SqlParameter("@TransportationCoasts", SqlDbType.Real);
            param[6].Value = tranCoast;

            param[7] = new SqlParameter("@FDay", SqlDbType.Date);
            param[7].Value = d;

            DA.ExecuteCmd("Fridges_Add", param);
            DA.Close();
        }

        public DataTable getTranMony()
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();

            DataTable dt = new DataTable();
            dt = DA.SelectData("TransMonyGer", null);

            DA.Close();

            return dt;
        }

        public void AddTranMony(int fid, string rid, double ammSp, double ammUs, double usTOsp)
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();
            SqlParameter[] param = new SqlParameter[5];

            param[0] = new SqlParameter("@TUSS", SqlDbType.Real);
            param[0].Value = ammUs;

            param[1] = new SqlParameter("@TSP", SqlDbType.Real);
            param[1].Value = ammSp;

            param[2] = new SqlParameter("@FID", SqlDbType.Int);
            param[2].Value = fid;

            param[3] = new SqlParameter("@UTS", SqlDbType.Real);
            param[3].Value = usTOsp;

            param[4] = new SqlParameter("@RID", SqlDbType.NVarChar,50);
            param[4].Value = rid;

            DA.ExecuteCmd("TransMonyAdd", param);
            DA.Close();
        }

        public void AddContent(int id, string type, int qty, double unitPrice)
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();
            SqlParameter[] param = new SqlParameter[4];

            param[0] = new SqlParameter("@FridgeId", SqlDbType.Int);
            param[0].Value = id;

            param[1] = new SqlParameter("@CType", SqlDbType.NVarChar,50);
            param[1].Value = type;

            param[2] = new SqlParameter("@Qty", SqlDbType.Int);
            param[2].Value = qty;

            param[3] = new SqlParameter("@UnitPrice", SqlDbType.Real);
            param[3].Value = unitPrice;

            DA.ExecuteCmd("FridgeContents_Add", param);
            DA.Close();
        }

        public DataTable getAll()
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();

            DataTable dt = new DataTable();
            dt = DA.SelectData("Fridges_GetAll", null);

            DA.Close();

            return dt;
        }

        public DataTable Print(int id)
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();
            SqlParameter[] param = new SqlParameter[1];

            param[0] = new SqlParameter("@Id", SqlDbType.Int);
            param[0].Value = id;

            DataTable dt = new DataTable();
            dt = DA.SelectData("Fridg_Print", param);

            DA.Close();

            return dt;
        }
        public DataTable Search(string Name)
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();
            SqlParameter[] param = new SqlParameter[1];

            param[0] = new SqlParameter("@Id", SqlDbType.NVarChar, 50);
            param[0].Value = Name;

            DataTable dt = new DataTable();
            dt = DA.SelectData("Fridges_Search", param);

            DA.Close();

            return dt;
        }

        public DataTable getContents(int id)
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();
            SqlParameter[] param = new SqlParameter[1];

            param[0] = new SqlParameter("@FridgeId", SqlDbType.Int);
            param[0].Value = id;

            DataTable dt = new DataTable();
            dt = DA.SelectData("FridgeContents_GetAll", param);

            DA.Close();

            return dt;
        }
    }
}
