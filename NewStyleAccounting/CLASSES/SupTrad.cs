﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace NewStyleAccounting.CLASSES
{
    class SupTrad
    {
        public void Add(string Name, string Phone, string Mobile, string SupAdd)
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();
            SqlParameter[] param = new SqlParameter[4];

            param[0] = new SqlParameter("@Name", SqlDbType.NVarChar, 50);
            param[0].Value = Name;

            param[1] = new SqlParameter("@Phone", SqlDbType.NVarChar, 50);
            param[1].Value = Phone;

            param[2] = new SqlParameter("@Mobile", SqlDbType.NVarChar, 50);
            param[2].Value = Mobile;

            param[3] = new SqlParameter("@SAdd", SqlDbType.NVarChar, 50);
            param[3].Value = SupAdd;

            DA.ExecuteCmd("HoleTraders_Add", param);
            DA.Close();
        }

        public void Update(string Name, string Phone, string Mobile, string SupAdd)
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();
            SqlParameter[] param = new SqlParameter[4];

            param[0] = new SqlParameter("@Name", SqlDbType.NVarChar, 50);
            param[0].Value = Name;

            param[1] = new SqlParameter("@Phone", SqlDbType.NVarChar, 50);
            param[1].Value = Phone;

            param[2] = new SqlParameter("@Mobile", SqlDbType.NVarChar, 50);
            param[2].Value = Mobile;

            param[3] = new SqlParameter("@Add", SqlDbType.NVarChar, 50);
            param[3].Value = SupAdd;

            DA.ExecuteCmd("SupTrad_Update", param);
            DA.Close();
        }

        public void pay(int id, double amm)
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();
            SqlParameter[] param = new SqlParameter[2];

            param[0] = new SqlParameter("@Gid", SqlDbType.Int);
            param[0].Value = id;

            param[1] = new SqlParameter("@PAmmount", SqlDbType.Real);
            param[1].Value = amm;

            DA.ExecuteCmd("HoleTrader_Out_Pay", param);
            DA.Close();
        }

        public DataTable getAll()
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();

            DataTable dt = new DataTable();
            dt = DA.SelectData("HoleTraders_GetAll", null);

            DA.Close();

            return dt;
        }

        public DataTable Search(string Name)
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();
            SqlParameter[] param = new SqlParameter[1];

            param[0] = new SqlParameter("@Name", SqlDbType.NVarChar, 50);
            param[0].Value = Name;

            DataTable dt = new DataTable();
            dt = DA.SelectData("HoleTraders_Search", param);

            DA.Close();

            return dt;
        }
    }
}
