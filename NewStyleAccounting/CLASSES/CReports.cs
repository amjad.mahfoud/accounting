﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace NewStyleAccounting.CLASSES
{
    class CReports
    {
        public DataTable getFieldBixes(DateTime from, DateTime to)
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();
            SqlParameter[] param = new SqlParameter[2];

            param[0] = new SqlParameter("@FromDate", SqlDbType.Date);
            param[0].Value = from;

            param[1] = new SqlParameter("@ToDate", SqlDbType.Date);
            param[1].Value = to;

            DataTable dt = new DataTable();
            dt = DA.SelectData("FieldBoxes_GetByDate", param);

            DA.Close();

            return dt;
        }
              
        public DataTable getPrePaid(DateTime from, DateTime to)
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();
            SqlParameter[] param = new SqlParameter[2];

            param[0] = new SqlParameter("@FromDate", SqlDbType.Date);
            param[0].Value = from;

            param[1] = new SqlParameter("@ToDate", SqlDbType.Date);
            param[1].Value = to;

            DataTable dt = new DataTable();
            dt = DA.SelectData("PrePaid_ByDate", param);

            DA.Close();

            return dt;
        }

        public DataTable getSuppliedByDate(DateTime from, DateTime to)
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();
            SqlParameter[] param = new SqlParameter[2];

            param[0] = new SqlParameter("@FromDate", SqlDbType.Date);
            param[0].Value = from;

            param[1] = new SqlParameter("@ToDate", SqlDbType.Date);
            param[1].Value = to;

            DataTable dt = new DataTable();
            dt = DA.SelectData("Supplies_ByDate", param);

            DA.Close();

            return dt;
        }

        public DataTable getFridgesByDate(DateTime from, DateTime to)
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();
            SqlParameter[] param = new SqlParameter[2];

            param[0] = new SqlParameter("@FromDate", SqlDbType.Date);
            param[0].Value = from;

            param[1] = new SqlParameter("@ToDate", SqlDbType.Date);
            param[1].Value = to;

            DataTable dt = new DataTable();
            dt = DA.SelectData("FridgesByDate", param);

            DA.Close();

            return dt;
        }

        // ExpencesByDate
        public DataTable getExpencesByDate(DateTime from, DateTime to)
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();
            SqlParameter[] param = new SqlParameter[2];

            param[0] = new SqlParameter("@FromDate", SqlDbType.Date);
            param[0].Value = from;

            param[1] = new SqlParameter("@ToDate", SqlDbType.Date);
            param[1].Value = to;

            DataTable dt = new DataTable();
            dt = DA.SelectData("ExpencesByDate", param);

            DA.Close();

            return dt;
        }

        public DataTable getSalByDate(DateTime from, DateTime to)
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();
            SqlParameter[] param = new SqlParameter[2];

            param[0] = new SqlParameter("@FromDate", SqlDbType.Date);
            param[0].Value = from;

            param[1] = new SqlParameter("@ToDate", SqlDbType.Date);
            param[1].Value = to;

            DataTable dt = new DataTable();
            dt = DA.SelectData("SelectSalByDate", param);

            DA.Close();

            return dt;
        }
                
        public DataTable getWaxedByDate(DateTime from, DateTime to)
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();
            SqlParameter[] param = new SqlParameter[2];

            param[0] = new SqlParameter("@FromDate", SqlDbType.Date);
            param[0].Value = from;

            param[1] = new SqlParameter("@ToDate", SqlDbType.Date);
            param[1].Value = to;

            DataTable dt = new DataTable();
            dt = DA.SelectData("WaxedByDate", param);

            DA.Close();

            return dt;
        }

        public DataTable getTransMonyByDate(DateTime from, DateTime to)
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();
            SqlParameter[] param = new SqlParameter[2];

            param[0] = new SqlParameter("@FromDate", SqlDbType.Date);
            param[0].Value = from;

            param[1] = new SqlParameter("@ToDate", SqlDbType.Date);
            param[1].Value = to;

            DataTable dt = new DataTable();
            dt = DA.SelectData("TransMonyByDate", param);

            DA.Close();

            return dt;
        }
        
        public DataTable getTotalProfit()
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();
            
            DataTable dt = new DataTable();
            dt = DA.SelectData("TotalProfit", null);

            DA.Close();

            return dt;
        }
    }
}
