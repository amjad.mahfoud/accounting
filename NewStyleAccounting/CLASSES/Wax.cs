﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace NewStyleAccounting.CLASSES
{
    class Wax
    {
        public void Add(double Amm, double per, double coast, string name)
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();
            SqlParameter[] param = new SqlParameter[4];

            param[0] = new SqlParameter("@Ammount", SqlDbType.Real);
            param[0].Value = Amm;

            param[1] = new SqlParameter("@WPercentage", SqlDbType.Real);
            param[1].Value = per;

            param[2] = new SqlParameter("@WaxingCoast", SqlDbType.Real);
            param[2].Value = coast;

            param[3] = new SqlParameter("@SourceName", SqlDbType.NVarChar, 50);
            param[3].Value = name;

            DA.ExecuteCmd("Waxed_add", param);
            DA.Close();
        }
        
        public DataTable getAll()
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();

            DataTable dt = new DataTable();
            dt = DA.SelectData("Waxed_get", null);

            DA.Close();

            return dt;
        }
        
    }
}
