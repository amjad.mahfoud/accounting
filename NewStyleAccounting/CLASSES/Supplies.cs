﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;


namespace NewStyleAccounting.CLASSES
{
    class Supplies
    {
        public void Add(string Name, DateTime d)
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();
            SqlParameter[] param = new SqlParameter[2];

            param[0] = new SqlParameter("@SupName", SqlDbType.NVarChar, 50);
            param[0].Value = Name;

            param[1] = new SqlParameter("@Sday", SqlDbType.Date);
            param[1].Value = d;

            DA.ExecuteCmd("Supplied_Add", param);
            DA.Close();
        }

        public DataTable print(int Id)
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();
            SqlParameter[] param = new SqlParameter[1];

            param[0] = new SqlParameter("@Id", SqlDbType.Int);
            param[0].Value = Id;
            DataTable dt = new DataTable();
            dt= DA.SelectData("Print_Goods_Contents", param);
            DA.Close();
            return dt;
        }
        public void AddContent(int Id, string Type, int Qty, double UnitPrice, double Dis)
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();
            SqlParameter[] param = new SqlParameter[5];

            param[0] = new SqlParameter("@SupId", SqlDbType.Int);
            param[0].Value = Id;

            param[1] = new SqlParameter("@GType", SqlDbType.NVarChar, 50);
            param[1].Value = Type;

            param[2] = new SqlParameter("@Quantity", SqlDbType.Int);
            param[2].Value = Qty;

            param[3] = new SqlParameter("@UnitPrice", SqlDbType.Real);
            param[3].Value = UnitPrice;

            param[4] = new SqlParameter("@DiscPer", SqlDbType.Real);
            param[4].Value = Dis;

            //
            DA.ExecuteCmd("SuppliesContents_Add", param);
            DA.Close();
        }

        public void WAX(int Id, double waxPer)
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();
            SqlParameter[] param = new SqlParameter[2];

            param[0] = new SqlParameter("@Id", SqlDbType.Int);
            param[0].Value = Id;

            param[1] = new SqlParameter("@WaxPer", SqlDbType.Real);
            param[1].Value = waxPer;

            DA.ExecuteCmd("Supplied_WAX", param);
            DA.Close();
        }

        public DataTable getAll()
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();

            DataTable dt = new DataTable();
            dt = DA.SelectData("Supplied_GetAll", null);

            DA.Close();

            return dt;
        }

        public DataTable getAllContents(int Id)
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();
            SqlParameter[] param = new SqlParameter[1];

            param[0] = new SqlParameter("@Id", SqlDbType.Int);
            param[0].Value = Id;

            DataTable dt = new DataTable();
            dt = DA.SelectData("SuppliedContents_GetAll_ForSup", param);

            DA.Close();

            return dt;
        }

        public DataTable getAllContents()
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();
            DataTable dt = new DataTable();
            dt = DA.SelectData("SuppliedContents_GetAll", null);

            DA.Close();

            return dt;
        }

        public DataTable Search(string Name)
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();
            SqlParameter[] param = new SqlParameter[1];

            param[0] = new SqlParameter("@Id", SqlDbType.NVarChar, 50);
            param[0].Value = Name;

            DataTable dt = new DataTable();
            dt = DA.SelectData("Supplied_Search", param);

            DA.Close();

            return dt;
        }

        public void Pay(int Id, double Ammount)
        {
            DBC.DataAccess DA = new DBC.DataAccess();

            DA.Open();
            SqlParameter[] param = new SqlParameter[2];

            param[0] = new SqlParameter("@Id", SqlDbType.Int);
            param[0].Value = Id;

            param[1] = new SqlParameter("@Ammount", SqlDbType.Real);
            param[1].Value = Ammount;

            DA.ExecuteCmd("Supplied_Pay", param);
            DA.Close();
        }
    }
}
